<?php

namespace App\Controller;

use App\Entity\Lieu;
use App\Form\LieuType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/lieu")
 * Class LieuController
 * @package App\Controller
 */
class LieuController extends AbstractController
{

    /**
     * Ajout d'un lieu
     * @Route ("/add/{fromPage}/{sortieId}", name="lieu_add")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $fromPage
     * @param $sortieId
     * @return Response
     */
    public function add(Request $request, EntityManagerInterface $em, $fromPage, $sortieId = null)
    {
        $lieu = new Lieu();
        $lieuForm = $this->createForm(LieuType::class, $lieu);
        $lieuForm->handleRequest($request);

        if ($lieuForm->isSubmitted() && $lieuForm->isValid()) {

            $em->persist($lieu);
            $em->flush();
            $this->addFlash('success', 'Le lieu a bien été rajouté');

            //En fonction de la page depuis laquelle on ajoute le lieu la redirection est différente
            if ($fromPage == 'create') {
                return $this->redirectToRoute('sortie_add');
            } else {
                return $this->redirectToRoute('sortie_update', ['id' => $sortieId]);
            }
        }

        return $this->render("lieu/add.html.twig", [
            'lieuForm' => $lieuForm->createView(),
            'fromPage' => $fromPage,
            'sortieId' => $sortieId
        ]);

    }
}
