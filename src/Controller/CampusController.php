<?php

namespace App\Controller;

use App\Entity\Campus;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CampusController
 * @package App\Controller
 * @Route ("/campus")
 */
class CampusController extends AbstractController
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Affichage de la liste des campus
     * @Route("/list", name="campus_list")
     * @param Request $request
     * @return Response
     */
    public function list(Request $request)
    {
        //Récupération du champs de recherche si données présentes
        $campusName = $request->request->get('name');

        //On récupère la liste des campus pour l'afficher
        $data = $this->getCampusList($campusName);
        return $this->render('campus/list.html.twig', [
            'campusList' => $data['campusList'],
            'host' => $data["host"],
            'campusName' => $data['campusName']
        ]);
    }


    /**
     * Actions d'ajout/modif/suppression
     * @Route("/manage/{action}/{id}", name="campus_manage",requirements={"id"="\d+"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $action
     * @param Campus|null $campus
     * @return JsonResponse
     */
    public function manage(Request $request, EntityManagerInterface $em, $action, Campus $campus = null)
    {
        //On vérifie qu'il s'agit bien d'un admin
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        //Récupération des données nécessaires
        $postData = $request->request->all();
        $newName = $postData["campusName"] ?? ''; //Pour création/modif
        $campusId = $postData["campusId"] ?? ''; //pour la suppression (id récupéré en POST, pas dans l'url)

        //On vérifie le format de l'id
        if ($campusId != '' && !is_numeric($campusId)) {
            throw new BadRequestHttpException("L'id du campus doit être au format numérique");
        }
        //On vérifie que le campus n'existe pas déjà
        $campusRepo = $this->getDoctrine()->getRepository(Campus::class);

        if ($action == 'update' || $action == 'add') {
            $campusWithSameName = $campusRepo->findCampusWithSameProperty('nom', $newName, $campusId);
            if (!empty($campusWithSameName)) {
                $notif = ['title' => 'Erreur ', 'content' => "Ce campus existe déjà", 'type' => 'error'];
                $status = 400;
            } else {
                if ($action == 'add') $campus = new Campus();
                try {
                    $campus->setNom($newName);
                    $em->persist($campus);
                    $em->flush();
                    $status = 200;
                    $message = ($action == 'add') ? "Le campus " . $campus->getNom() . " a bien été ajouté" : "Le campus a bien été modifié";
                    $notif = ['title' => 'Succès ', 'content' => $message];
                } catch (PDOException $e) {
                    $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
                    $status = 500;
                    $notif = ['title' => 'Erreur', 'content' => "Erreur lors de l'ajout. Veuillez vous rapprocher de l'administrateur"];
                }
            }
        } else if ($action == 'delete') {
            $campusToDelete = $campusRepo->find($campusId);
            if (empty($campusToDelete)) {
                throw $this->createNotFoundException("Le campus à supprimer n'a pas été trouvée en base");
            }
            try {
                $em->remove($campusToDelete);
                $em->flush();
                $status = 200;
                $notif = ['title' => 'Suppression réussie', 'content' => "Le campus " . $campusToDelete->getNom() . ' a bien été supprimé'];
            } catch (PDOException $e) {
                $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
                $status = 500;
                $notif = ['title' => 'Erreur', 'content' => "Erreur lors de la suppression. Veuillez vous rapprocher de l'administrateur"];
            }
        }
        //Nécessaire pour l'actualisation de la table après ajout/modif/suppression
        $data = $this->getCampusList();
        $table = $this->renderView('campus/list_table.html.twig', $data);

        //Retour Json avec le statut de la réponse, la table actualisée et la notification (message erreur/succès)
        return new JsonResponse([
            'status' => $status ?? 200,
            'table' => $table,
            'notif' => $notif ?? '',
        ]);
    }

    /**
     * Récupération de la liste des campus
     * @param string $campusName
     * @return array
     */
    private function getCampusList($campusName = '')
    {
        //On récupère soit tous les campus soit ceux rattachés au nom
        $campusRepo = $this->getDoctrine()->getRepository(Campus::class);
        if ($campusName == '') {
            $campusList = $campusRepo->findBy([], ['id' => 'ASC']);
        } else {
            $campusList = $campusRepo->findCampusByName($campusName);
        }
        $host = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REDIRECT_BASE'];
        return array(
            'campusList' => $campusList,
            'host' => $host,
            'campusName' => $campusName
        );
    }

}
