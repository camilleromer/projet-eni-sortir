<?php

namespace App\Controller;

use App\Entity\Etat;
use App\Entity\Inscription;
use App\Entity\Lieu;
use App\Entity\Sortie;
use App\Entity\Ville;
use App\Form\SortieType;
use App\Services\SortieManager;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SortieController
 * @package App\Controller

 */
class SortieController extends AbstractController
{
    private $session;
    private $logger;

    public function __construct(SessionInterface $session, LoggerInterface $logger)
    {
        $this->session = $session;
        $this->logger = $logger;
    }

    /**
     * Affichage des sorties (page d'accueil une fois connecté)
     * @Route("/", name="sortie_list")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param SortieManager $sortieManager
     * @return Response
     */
    public function list(Request $request, EntityManagerInterface $em, SortieManager $sortieManager)
    {
        //Récupération des filtres
        $postData = $request->request->all();

        //Si on n'a pas de données postées mais qu'on a des filtres déjà en session on les récupère
        if ($postData == null) {
            if ($this->session->get('filters') != null) {
                $postData = $this->session->get('filters');
            }
        }
        //On détruit la session qui gardait les valeurs des saisies pour modif/création sortie
        //Ne servait qu'à garder la saisie si erreur, quand on revient sur cette page on repart de 0
        $this->session->remove('dataFormSortie');

        //On récupère la liste des sorties
        $data = $sortieManager->getSortiesList($postData, $em);

        //On sauvegarde nos filtres en session pour éviter de les ressaisir après rechargement de la page
        $this->session->set('filters', $postData);

        return $this->render('sortie/list.html.twig', $data);
    }

    /**
     * @Route("/sortie/details/{id}", name="sortie_details", requirements={"id":"\d+"})
     * @param Sortie $sortie
     * @return Response
     */
    public function details(Sortie $sortie)
    {
        //Création du form de sortie (qui sera seulement en lecture)
        $sortieForm = $this->createForm(SortieType::class, $sortie, ['property_path' => 'add']);

        return $this->render('sortie/details.html.twig', [
            'sortie' => $sortie,
            'sortieForm' => $sortieForm->createView()
        ]);
    }

    /**
     * Ajout d'une sortie
     * @Route("/sortie/add", name="sortie_add")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param SortieManager $sortieManager
     * @return Response
     */
    public function add(Request $request, EntityManagerInterface $em, SortieManager $sortieManager)
    {
        $sortie = new Sortie();
        $sortieForm = $this->createForm(SortieType::class, $sortie);
        $sortieForm->handleRequest($request);

        //Sur soumission du formulaire et si valide
        if ($sortieForm->isSubmitted() && $sortieForm->isValid()) {
            $sortie->setCampus($this->getUser()->getCampus()); //On met le campus utilisateur par défaut
            //On met les résultats en session pour éviter de les ressaisir après une erreur
            $this->session->set('dataFormSortie', $sortieForm->getData());

           // $redirectTo = $this->manageSortie('create', $sortie, $em, $request, $sortieForm);
            $redirectTo = $sortieManager->manageSortie('create', $sortie, $em, $request, $sortieForm);
            $route = $redirectTo['name'];
            $params = $redirectTo['params'];
            return $params != null ? $this->redirectToRoute($route, $params) : $this->redirectToRoute($route);
        }
        //Contruction début URL (pour la construction des url pour mes actions AJAX)
        $host = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REDIRECT_BASE'];

        //On affiche le formulaire
        return $this->render('sortie/add.html.twig', [
            'sortieForm' => $sortieForm->createView(),
            'host' => $host,
            'selectedValues' => $this->session->get('dataFormSortie') ?? ''
        ]);
    }

    /**
     * @Route("/sortie/update/{id}", name="sortie_update",  requirements={"id":"\d+"})
     * @param Sortie $sortie
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param SortieManager $sortieManager
     * @return RedirectResponse|Response
     */
    public function update(Sortie $sortie, Request $request, EntityManagerInterface $em, SortieManager $sortieManager)
    {
        //Au delà du contrôle côté affichage on revérifie ici que c'est bien l'organisateur et que la sortie n'a pas été publiée
        if ($this->getUser() != $sortie->getOrganisateur() || $sortie->getEtat() != $this->getStateObject('Créée')) {
            //Si ce n'est pas le cas on redirige versl a page d'accueil
            return $this->redirectToRoute('sortie_list');
        }
        //Création du formulaire
        $sortieForm = $this->createForm(SortieType::class, $sortie);
        $sortieForm->handleRequest($request);

        //Sur soumission du formulaire
        if ($sortieForm->isSubmitted() && $sortieForm->isValid()) {
            //On met les résultats en session pour éviter de tout ressaisir si erreur
            $this->session->set('dataFormSortie', $sortieForm->getData());
            //On effectue l'action liée au bouton sur lequel l'utilisateur a appuyé (enregistrer/publier/supprimer)
           // $redirectTo = $this->manageSortie('update', $sortie, $em, $request, $sortieForm);
            $redirectTo = $sortieManager->manageSortie('update', $sortie, $em, $request, $sortieForm);

            $route = $redirectTo['name'];
            $params = $redirectTo['params'];

            return $params != null ? $this->redirectToRoute($route, $params) : $this->redirectToRoute($route);
        }
        //Contruction début URL
        $host = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REDIRECT_BASE'];

        return $this->render('sortie/update.html.twig', [
            'sortieForm' => $sortieForm->createView(),
            'host' => $host,
            'selectedValues' => $this->session->get('dataFormSortie') ?? '',
            'sortie' => $sortie
        ]);
    }

    /**
     * Affiche les informations du lieu en fonction du lieu sélectionné
     * @Route ("/sortie/displayInfosLieu/{id}", name="sortie_display_infos_lieu")
     * @param Lieu $lieu
     * @return string
     */
    public function displayInfosLieu(Lieu $lieu)
    {
        return new JsonResponse([
            'success' => true,
            'lieu' => $lieu
        ]);
    }

    /**
     * Publication de la sortie
     * @Route ("/sortie/publish/{id}/{ajax}", name="sortie_publish", requirements={"id":"\d+"})
     * @param Sortie $sortie
     * @param EntityManagerInterface $em
     * @param SortieManager $sortieManager
     * @param bool $ajax
     * @return JsonResponse|RedirectResponse
     */
    public function publish(Sortie $sortie, EntityManagerInterface $em, SortieManager $sortieManager, $ajax = false)
    {
        //Au delà du contrôle côté affichage on revérifie ici que c'est bien l'organisateur et que la sortie n'a pas été publiée
        if ($this->getUser() != $sortie->getOrganisateur() || $sortie->getEtat() != $this->getStateObject('Créée')) {
            if ($ajax) {
                $notif = ['title' => 'Erreur', 'content' => "Vous n'avez pas le droit de publier cette sortie"];
                $status = 403;
                //Retour Json avec le statut de la réponse et la notification (message erreur/succès)
                return new JsonResponse([
                    'status' => $status,
                    'notif' => $notif,
                ]);
            } else {
                $this->addFlash("danger", "Vous n'avez pas le droit de publier cette sortie");
                return $this->redirectToRoute('sortie_list');
            }
        }
        //On change l'état de créée à ouverte
        $sortie->setEtat($this->getStateObject('Ouverte'));

        try {
            $em->persist($sortie);
            $em->flush();

            if ($ajax) {
                $notif = ['title' => 'Publication réussie', 'content' => "La sortie " . $sortie->getNom() . " a été publiée"];
                $status = 200;
                //On récupère les filtres gardés en session (pour ne pas perdre les choix de l'utilisateur)
                $filters = $this->session->get('filters');

                //On récupère la liste des sorties
                $data = $sortieManager->getSortiesList($filters, $em);
                $table = $this->renderView('sortie/list_table.html.twig', $data);

                //Retour Json avec le statut de la réponse, la table actualisée et la notification (message erreur/succès)
                return new JsonResponse([
                    'status' => $status,
                    'table' => $table,
                    'notif' => $notif,
                ]);
            } else {
                $this->addFlash("success", "La sortie " . $sortie->getNom() . ' a bien été publiée');
            }

        } catch (PDOException $e) {
            $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
        }
        //Redirection vers la liste des sorties (page d'accueil)
        return $this->redirectToRoute("sortie_list");
    }

    /**
     * Route d'inscription à une sortie
     * @Route("/sortie/subscribe/{id}", name="sortie_subscribe", requirements={"id":"\d+"})
     * @param Sortie $sortie
     * @param EntityManagerInterface $em
     * @param SortieManager $sortieManager
     * @return string
     */
    public function subscribe(Sortie $sortie, EntityManagerInterface $em, SortieManager $sortieManager)
    {
        //Vérification que la sortie a le statut 'Ouverte' et que la date de clôture n'est pas dépassée et que le nombre total d'inscription n'a pas été atteint
        //Les vérifs sont faites côté client, vérif supplémentaire ici pour plus de sécurité
        $today = new \DateTime();

        //On récupère les inscriptions => permet de comparer le nombre d'inscription au nombre max de place pour la sortie
        $inscriptionRepo = $this->getDoctrine()->getRepository(Inscription::class);
        $inscriptionsRelatedToSorties = $inscriptionRepo->findBy(['sortie' => $sortie]);

        if ($sortie->getEtat()->getId() != 2 || $today > $sortie->getDateCloture() || count($inscriptionsRelatedToSorties) == $sortie->getNbInscriptionsMax()) {
            $notif = ['title' => 'Erreur', 'content' => "Il n'est plus possible de s'inscrire pour cette sortie "];
            $status = 403;
        } else {
            $inscription = new Inscription();
            $inscription->setSortie($sortie);
            $inscription->setParticipant($this->getUser());
            $inscription->setDateInscription($today);
            try {
                $em->persist($inscription);
                $em->flush();
                $notif = ['title' => 'Inscription réussie', 'content' => 'Vous venez de vous inscrire à la sortie ' . $sortie->getNom()];
            } catch (PDOException $e) {
                $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
            }
        }
        //On récupère les filtres gardés en session (pour ne pas perdre les choix de l'utilisateur)
        $filters = $this->session->get('filters');
        //On récupère la liste des sorties
        $data = $sortieManager->getSortiesList($filters, $em);
        $table = $this->renderView('sortie/list_table.html.twig', $data);

        //Retour Json avec le statut de la réponse, la table actualisée et la notification (message erreur/succès)
        return new JsonResponse([
            'status' => $status ?? 200,
            'table' => $table,
            'notif' => $notif ?? '',
        ]);
    }

    /**
     * @Route("/sortie/quit/{id}", name="sortie_quit",  requirements={"id":"\d+"}, methods={"GET"})
     * @param Sortie $sortie
     * @param EntityManagerInterface $em
     * @param SortieManager $sortieManager
     * @return JsonResponse
     */
    public function quit(Sortie $sortie, EntityManagerInterface $em, SortieManager $sortieManager)
    {
        //On vérifie que la sortie n'a pas débutée (sinon pas possible de se désister)
        $today = new \DateTime();
        if ($today > $sortie->getDateDebut()) {
            $notif = ['title' => 'Erreur', 'content' => "Il n'est plus possible de se désister car la sortie a débuté !"];
            $status = 403;
        } else {
            //Si ok, on va supprimer l'inscription correspondante à l'utilisateur et la sortie
            $inscRepo = $this->getDoctrine()->getRepository(Inscription::class);
            $inscriptionToDelete = $inscRepo->findOneBy(array('sortie' => $sortie, 'participant' => $this->getUser()));
            try {
                $em->remove($inscriptionToDelete);
                $em->flush();
                $notif = ['title' => 'Désinscription réussie', 'content' => ' Vous n\'êtes désormais plus inscrit à la sortie ' . $sortie->getNom()];
            } catch (PDOException $e) {
                $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
            }
        }
        //On récupère les filtres gardés en session (pour ne pas perdre les choix de l'utilisateur)
        $filters = $this->session->get('filters');
        //On récupère la liste des sorties
        $data = $sortieManager->getSortiesList($filters, $em);
        $table = $this->renderView('sortie/list_table.html.twig', $data);

        //Retour Json avec le statut de la réponse, la table actualisée et la notification (message erreur/succès)
        return new JsonResponse([
            'status' => $status ?? 200,
            'table' => $table,
            'notif' => $notif ?? '',
        ]);
    }

    /**
     * @Route("/sortie/cancelForm/{id}", name="sortie_cancel_form",  requirements={"id":"\d+"})
     * @param Sortie $sortie
     * @return Response
     */
    public function cancelForm(Sortie $sortie)
    {
        return $this->render('sortie/cancel_form.html.twig', [
            'sortie' => $sortie
        ]);
    }

    /**
     * @Route("/sortie/cancel/{id}", name="sortie_cancel",  requirements={"id":"\d+"})
     * @param Sortie $sortie
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse
     */
    public function cancel(Sortie $sortie, Request $request, EntityManagerInterface $em)
    {
        //On vérifie que c'est bien l'organisateur de la sortie ou un admin qui effectue l'action d'annulation
        if ($sortie->getOrganisateur() == $this->getUser() || $this->isGranted('ROLE_ADMIN')) {
            //On vérifie que la sortie n'a pas encore débutée (elle doit etre soit ouverte soit clôturée pour être annulée)
            if ($sortie->getEtat()->getId() == 2 || $sortie->getEtat()->getId() == 3) {

                //On insère le motif d'annulation en base et on change l'état à Annulée
                $motif = trim(htmlspecialchars($request->request->get('motif')));
                $sortie->setMotifAnnulation($motif);
                $sortie->setEtat($this->getStateObject('Annulée'));
                try {
                    $em->persist($sortie);
                    $em->flush();
                    $this->addFlash('success', 'La sortie ' . $sortie->getNom() . ' a bien été annulée');
                } catch (PDOException $e) {
                    $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
                }
            } else {
                $this->addFlash('danger', 'Cette sortie ne peut être annulée');
            }
        } else {
            $this->addFlash('danger', "Vous n'avez pas les droits pour annuler cette sortie");
        }
        return $this->redirectToRoute('sortie_list');
    }

    /**
     * Alimentation du select des lieux en fonction de la ville sélectionnée
     * @Route ("/sortie/getLieuSelectValues/{id}", name="sortie_get_lieu_select_values", defaults={null})
     * @param Ville $ville
     * @return JsonResponse
     */
    public function getLieuSelectValues(Ville $ville)
    {
        $lieux = [];
        foreach ($ville->getLieux() as $lieu) {
            $lieuToAdd['id'] = $lieu->getId();
            $lieuToAdd['nom'] = $lieu->getNom();
            $lieux[] = $lieuToAdd;
        }
        return new JsonResponse([
            'lieux' => $lieux
        ]);
    }

    /**
     * Récupération de l'ensemble des sorties en fonction des filtres sélectionnés
     * @param $postData
     * @param $em
     * @return array
     */
  /*  private function getSortiesList($postData, $em)
    {
        //On construit le tableau de filtres avec les données postées
        $filters['campus'] = $postData['campus'] ?? $this->getUser()->getCampus()->getId(); //Campus du participant par défaut
        $filters['nom'] = $postData['nom'] ?? null;
        $filters['date_debut'] = $postData['date_debut'] ?? "";
        $filters['date_fin'] = $postData['date_fin'] ?? "";
        $filters['isOrganizer'] = (empty($postData)) ? true : ($postData['isOrganizer']) ?? false;
        $filters['isRegistered'] = (empty($postData)) ? true : ($postData['isRegistered']) ?? false;
        $filters['isNotRegistered'] = (empty($postData)) ? true : ($postData['isNotRegistered']) ?? false;
        $filters['passedOutings'] = (empty($postData)) ? false : ($postData['passedOutings']) ?? false;

        //Récupération de la liste des campus pour les filtres
        $campusRepo = $this->getDoctrine()->getRepository(Campus::class);
        $campus = $campusRepo->findAll();

        //Récupération des sorties en fonction des filtres
        $sortieRepo = $this->getDoctrine()->getRepository(Sortie::class);
        $sorties = $sortieRepo->findSortiesWithFilters($filters, $this->getUser());

        //Mise à jour des états des sorties (en cours, passée, clôturée)
        $this->updateSortiesStates($sorties, $em);
        $host = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REDIRECT_BASE'];

        return array(
            'listCampus' => $campus,
            'sorties' => $sorties,
            'selectedValues' => $filters,
            'host' => $host
        );
    }*/

    /**
     * Traitement des actions accessibles via les pages d'ajout/modif d'une sortie
     * @param $fromPage
     * @param $sortie
     * @param $em
     * @param $request
     * @param $sortieForm
     * @return array[]
     */
   /* private function manageSortie($fromPage, $sortie, $em, $request, $sortieForm)
    {
        //On récupère l'action (Enregistrer / publier / supprimer)
        $action = $request->request->get('btnSubmit');
        //On redirigera sur la liste des sorties par défaut
        $redirectTo = ['name' => 'sortie_list', 'params' => null];

        //Vérification des dates
        $today = time();
        $dateClotureWithSeconds = $sortieForm->getData()->getDateCloture()->format('Y-m-d') . " 23:59:59"; //On donne jusqu'à la fin de la journée
        $dateCloture = strtotime($dateClotureWithSeconds);
        $dateDebut = strtotime($sortieForm->getData()->getDateDebut()->format('Y-m-d H:i:s'));

        $errorMessage = '';
        //La date de début ne doit pas être inférieure à la date du jour
        if ($dateDebut < $today) {
            $errorMessage = "La date de la sortie ne peut pas être inférieure à la date actuelle";
            //La date de clôture doit se trouver entre la date du jour et la date de début
        } else if ($dateCloture > $dateDebut || $dateCloture < $today) {
            $errorMessage = "La date de clôture doit se situer entre la date du jour et la date de début de la sortie";
        }
        if ($errorMessage != '') {
            $this->addFlash('danger', $errorMessage);
            $routeName = ($fromPage == 'update') ? 'sortie_update' : 'sortie_add';
            $routeParams = ($fromPage == 'update') ? ['id' => $sortie->getId()] : null;
            $redirectTo = ['name' => $routeName, 'params' => $routeParams];
            return $redirectTo;
        }

        //Traitement en fonction de l'action à effectuer
        switch ($action) {

            //BOUTON D'ENREGISTREMENT
            case 'save':
                if ($fromPage == 'create') {
                    $sortie->setOrganisateur($this->getUser());
                    $sortie->setEtat($this->getStateObject('Créée'));
                }
                //Enregistrement des résultats
                try {
                    $em->persist($sortie);
                    $em->flush();
                    $message = ($fromPage == 'create') ? 'La sortie a bien été enregistrée, pensez à la publier' : 'La sortie a bien été modifiée';
                    $this->addFlash('success', $message);
                } catch (PDOException $e) {
                    $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
                }
                break;

            //BOUTON DE PUBLICATION
            case 'publish':
                //Enregistrement d'abord des résultats
                if ($fromPage == 'create') {
                    $sortie->setOrganisateur($this->getUser());
                    $sortie->setEtat($this->getStateObject('Créée'));
                }
                try {
                    $em->persist($sortie);
                    $em->flush();
                    //Puis redirection vers la route de publication
                    $redirectTo = ['name' => 'sortie_publish', 'params' => ['id' => $sortie->getId()]];
                } catch (PDOException $e) {
                    $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
                }
                break;

            //BOUTON DE SUPPRESSION (Suppression en base ici contrairement à l'annulation)
            case 'delete':
                //On récupère l'id dans le champ caché (suppression en POST car plus sécure)
                $sortieId = $request->request->get('sortieId') ?? null;
                //On vérifie le format de l'id
                if (!is_numeric($sortieId)) {
                    throw new BadRequestHttpException("L'id de la sortie doit être au format numérique");
                }
                //On récupère la sortie correspondante en base (on vérifie son existence)
                $sortieRepo = $this->getDoctrine()->getRepository(Sortie::class);
                $sortieToDelete = $sortieRepo->find($sortieId);
                if (empty($sortieToDelete)) {
                    throw $this->createNotFoundException("La sortie a supprimer n'a pas été trouvée en base");
                }
                try {
                    $em->remove($sortieToDelete);
                    $em->flush();
                    $this->addFlash('success', 'La sortie ' . $sortieToDelete->getNom() . ' a bien été supprimée');
                } catch (PDOException $e) {
                    $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
                }
                break;
        }
        //On renvoit le nom et les paramètres de la route vers lequel on redirigera l'utilisateur
        return $redirectTo;
    }
*/

    /**
     * Renvoie l'objet état à partir de son libelle
     * @param $libelle
     * @return Etat|object|null
     */
    private function getStateObject($libelle)
    {
        $etatRepo = $this->getDoctrine()->getRepository(Etat::class);
        $etat = $etatRepo->findOneBy(['libelle' => $libelle]);
        if (empty($etat)) {
            throw $this->createNotFoundException("La sortie a supprimer n'a pas été trouvée en base");
        }
        return $etat;
    }

    /**
     * Met à jour les états des sorties en fonction de la date actuelle
     * @param $sorties
     * @param $em
     */
    private function updateSortiesStates($sorties, $em)
    {
        $today = time();

        //Pour chaque sortie on compare la date actuelle avec les dates liées à la sortie
        foreach ($sorties as $sortie) {

            $newEtat = null;
            $dateClotureWithSeconds = $sortie->getDateCloture()->format('Y-m-d') . "23:59:59";
            $dateCloture = strtotime($dateClotureWithSeconds);
            //$dateCloture = strtotime($sortie->getDateCloture()->format('Y-m-d'));
            $dateDebut = strtotime($sortie->getDateDebut()->format('Y-m-d H:i:s'));
            $dateFin = $dateDebut + ($sortie->getDuree() * 60);

            $nbInscrits = count($sortie->getInscriptions()); //On récupère le nombre d'inscrits
            //Si la sortie est annulée on la laisse comme ça on ne met pas à jour (sinon repasse à son état initial)
            if ($sortie->getEtat()->getId() != 6) {
                try {
                    //Si date du jour entre la date de début et de fin de sortie => EN COURS
                    if ($today > $dateDebut && $today < $dateFin) {
                        $newEtat = $this->getStateObject('En cours');
                    }
                    //Si on a dépassé la date de fin => PASSEE
                    if ($today > $dateFin) {
                        $newEtat = $this->getStateObject('Passée');
                    }
                    //Si on a dépassé la date limite d'inscription et que la sortie n'a pas commence => CLOTUREE
                    //Ou si le nombre max de place a été atteint
                    if ($today > $dateCloture && $today < $dateDebut || $nbInscrits == $sortie->getNbInscriptionsMax()) {
                        $newEtat = $this->getStateObject('Cloturée');
                    }

                    //Si jamais une personne s'est désinscrite entre temps (et que la date n'est pas dépassée on repasse de l'état clôturé à ouvert)
                    if ($sortie->getEtat()->getId() == 3 && $nbInscrits < $sortie->getNbInscriptionsMax() && $today < $dateCloture) {
                        $newEtat = $this->getStateObject('Ouverte');
                    }
                } catch (Exception $e) {
                }
                //On met à jour l'état en base
                if ($newEtat != null) {

                    $sortie->setEtat($newEtat);
                    $em->persist($sortie);
                    $em->flush();
                }
            }
        }
    }
}
