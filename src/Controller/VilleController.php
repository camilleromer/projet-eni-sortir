<?php

namespace App\Controller;

use App\Entity\Ville;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class VilleController
 * @package App\Controller
 * @Route ("/ville")
 */
class VilleController extends AbstractController
{
    private $logger;
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/list", name="ville_list")
     * @param Request $request
     * @return Response
     */
    public function list(Request $request)
    {
        //Récupération du champs de recherche si données présentes
        $villeName = $request->request->get('name') ?? '';
        //On récupère soit toutes les villes soit ceux rattachés au nom
        $data = $this->getVillesList($villeName);

        return $this->render('ville/list.html.twig', [
            'villesList' => $data['villesList'],
            'host' => $data['host'],
            'villeName' => $data["villeName"],
        ]);
    }
    /**
     * @Route("/manage/{action}/{id}", name="ville_manage",requirements={"id"="\d+"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $action
     * @param Ville|null $ville
     * @return JsonResponse
     */
    public function manage(Request $request, EntityManagerInterface $em, $action, Ville $ville = null)
    {
        //Récupération des données nécessaires
        $postData = $request->request->all();
        $newName = $postData["nomVille"] ?? '';
        $cp = isset($postData["cpVille"]) ? filter_var($postData["cpVille"], FILTER_VALIDATE_REGEXP, ['options' => array("regexp" => "/^\d{5}$/")]) : '';
        $villeId = $postData["villeId"] ?? ''; //Pour la suppression (id en post)

        //On vérifie que la ville n'existe pas déjà
        $villeRepo = $this->getDoctrine()->getRepository(Ville::class);

        if ($action == 'update' || $action == 'add') {
            $id = ($action == 'update' ) ? $ville->getId() : '';
            $sameNameCity = $villeRepo->findVilleWithSameProperty('nom', $newName, $id);
            $sameCp = $villeRepo->findVilleWithSameProperty('codepostal', $cp, $id);

            if ($cp == '') {
                $notif = ['title' => 'Erreur ', 'content' => "Veuillez rentrer un code postal valide", 'type' => 'error'];
                $status = 400;

            }else if(!empty($sameNameCity)  || !empty($sameCp)){
                $notif = ['title' => 'Erreur ', 'content' => "Cette ville existe déjà", 'type' => 'error'];
                $status = 400;

            }else{
                if ($action == 'add') $ville = new Ville();
                try {
                    $ville->setNom($newName);
                    $ville->setCodepostal($cp);
                    $em->persist($ville);
                    $em->flush();
                    $status = 200;
                    $message = ($action == 'add') ? "La ville a bien été ajoutée" : "La ville a bien été modifée";
                    $notif = ['title' => 'Succès ', 'content' => $message];
                } catch (PDOException $e) {
                    $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
                    $status = 500;
                    $notif = ['title' => 'Erreur', 'content' => "Erreur lors de l'ajout. Veuillez vous rapprocher de l'administrateur"];
                }
            }
        } else if ($action == 'delete') {

            //On vérifie le format de l'id
            if ($villeId != '' && !is_numeric($villeId)) {
                throw new BadRequestHttpException("L'id de la ville doit être au format numérique");
            }
            $villeToDelete = $villeRepo->find($villeId);

            if (empty($villeToDelete)) {
                throw $this->createNotFoundException("La ville à supprimer n'a pas été trouvée en base");
            }
            try {
                $em->remove($villeToDelete);
                $em->flush();
                $status = 200;
                $notif = ['title' => 'Suppression réussie', 'content' => "La ville " . $villeToDelete->getNom() . ' a bien été supprimé'];

            } catch (PDOException $e) {
                $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
                $status = 500;
                $notif = ['title' => 'Erreur', 'content' => "Erreur lors de la suppression. Veuillez vous rapprocher de l'administrateur"];
            }
        }
        //Nécessaire pour l'actualisation de la table une fois ville supprimé
        $data = $this->getVillesList();
        $table = $this->renderView('ville/list_table.html.twig', $data);

        //Retour Json avec le statut de la réponse, la table actualisée et la notification (message erreur/succès)
        return new JsonResponse([
            'status' => $status ?? 200,
            'table' => $table,
            'notif' => $notif ?? ''
        ]);
    }

    /**
     * Récupération de la liste des villes
     * @param $villeName
     * @return array
     */
    private function getVillesList($villeName=''){
        //On récupère soit toutes les villes soit ceux rattachés au nom
        $villeRepo = $this->getDoctrine()->getRepository(Ville::class);
        if ($villeName == '') {
            $villesList = $villeRepo->findBy([], ['id' => 'ASC']);
        } else {
            $villesList = $villeRepo->findVillesByName($villeName);
        }
        $host = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REDIRECT_BASE'];
        return [
            'villesList' => $villesList,
            'host' => $host,
            'villeName' => $villeName,
        ];
    }
}
