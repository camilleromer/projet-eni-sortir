<?php

namespace App\Controller;

use App\Entity\Campus;
use App\Entity\Participant;
use App\Form\ParticipantType;
use App\Services\FileUploader;
use App\Services\Mailer;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class ParticipantController extends AbstractController
{
    private $logger;
    private $encoder;

    public function __construct(LoggerInterface $logger, UserPasswordEncoderInterface $encoder)
    {
        $this->logger = $logger;
        $this->encoder = $encoder;
    }

    /**
     * Route de Login (page sur laquelle on arrive par défaut)
     * @Route ("/login", name="login")
     * @param AuthenticationUtils $authUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authUtils)
    {
        //On arrive par défaut sur la page de login, mais si l'utilisateur est déjà connecté
        //on redirige vers la page d'accueil avec la liste des sorties
        if ($this->getUser() != null) {
            return $this->redirectToRoute("sortie_list");
        }

        //Contrôle des identifiants pour un form de login
        $error = $authUtils->getLastAuthenticationError();
        $lastUsername = $authUtils->getLastUsername();

        //Affichage du formulaire
        return $this->render('participant/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error
        ));
    }

    /**
     * Route de modification/ajout d'un participant
     * @Route ("/participant/manage/{action}", name="participant_manage")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param FileUploader $fileUploader
     * @param string $action
     * @return Response
     */
    public function manage(Request $request, EntityManagerInterface $em, FileUploader $fileUploader, $action)
    {
        if($action == 'add'){
            $this->denyAccessUnlessGranted("ROLE_ADMIN");  //Seul l'admin peut ajouter un utilisateur
        }
        $userToUpdate =  ($action == 'update')  ?  $this->getUser() : new Participant();
        $viewToRender = ($action == 'update')  ?  'participant/update_participant.html.twig' :'participant/add.html.twig';

        $userForm = $this->createForm(ParticipantType::class, $userToUpdate);
        $userForm->handleRequest($request); //Hydratation des données

        //Récupération de la liste des participants (pour l'interface admin)
        $userRepo = $this->getDoctrine()->getRepository(Participant::class);
        $participants = $userRepo->getUsersExceptHimself($this->getUser());

        if ($userForm->isSubmitted() && $userForm->isValid()) {
            //On hashe le mot de passe avant de l'insérer en base
            $password = $this->encoder->encodePassword($userToUpdate, $userToUpdate->getPassword());
            $userToUpdate->setPassword($password);

            //Récupération de la photo et stockage du nom en base
            $file = $userForm->get('photo')->getData();
            if ($file) {
                $filename = $fileUploader->upload($file);
                $userToUpdate->setPhoto($filename);
            }
            if ($action == 'add') {
                $userToUpdate->setActif(true);
                $userToUpdate->setRoles(['ROLE_USER']);
            }
            $em->persist($userToUpdate);
            $em->flush();

            //Message de succès et redirection
            if ($action == 'update') {
                $this->addFlash("success", "Votre profil a été mis à jour");
                return $this->redirectToRoute("participant_manage", array('action' => 'update'));
            } else {
                $this->addFlash("success", 'Le participant ' . $userToUpdate->getNom() . ' a bien été ajouté');
                return $this->redirectToRoute("participant_manage", array('action' => 'add'));
            }
        }
        $host = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REDIRECT_BASE'];
        return $this->render($viewToRender, [
            "userToUpdate" => $userToUpdate,
            "userForm" => $userForm->createView(),
            'host' => $host,
            "participants" => $participants
        ]);
    }

    /**
     * @Route ("/admin/participant/addFromCsv", name="participant_add_from_csv")
     * @param Request $request
     * @param FileUploader $fileUploader
     * @param EntityManagerInterface $em
     * @return RedirectResponse
     */
    public function addFromCsv(Request $request, FileUploader $fileUploader, EntityManagerInterface $em)
    {
        //Récup fichier
        $file = $request->files->get('usersCsv');

        if ($file) {
            $filename = $fileUploader->upload($file);
            $path = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REDIRECT_BASE'];
            //Lecture
            $fileToRead = $path . '/uploads/' . $filename;
            $csv = $this->read($fileToRead);

            $campusRepo = $this->getDoctrine()->getRepository(Campus::class);

            //Ajout du participant avec les données récupérées
            foreach ($csv as $line) {
                $line = explode(';', $line[0]);

                if(count($line) > 1) {
                    $participant = new Participant();
                    $participant->setPseudo($line[0]);
                    $participant->setNom($line[1]);
                    $participant->setPrenom($line[2]);
                    $participant->setTelephone($line[3]);
                    $participant->setEmail($line[4]);
                    $participant->setPassword($this->encoder->encodePassword($participant, $line[5]));
                    $participant->setPhoto($line[6]);
                    $campus = $campusRepo->find($line[7]);
                    $participant->setCampus($campus);
                    $participant->setActif(true);
                    $participant->setRoles(['ROLE_USER']);
                    try {
                       $em->persist($participant);
                    } catch (PDOException $e) {
                        $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
                    }
                }
            }
            $em->flush();
        }
        $this->addFlash('success', 'Le ou les participants ont bien été ajoutés');
        return $this->redirectToRoute('participant_manage', array('action' => 'add'));
    }

    /**
     * Route permettant d'effectuer la demande d'un nouveau mot de passe
     * @Route ("/login/newPassRequest", name="participant_new_pass_request")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param Mailer $mailer
     * @param TokenGeneratorInterface $tokenGenerator
     * @return Response
     */
    public function newPassRequest(Request $request, EntityManagerInterface $em, Mailer $mailer, TokenGeneratorInterface $tokenGenerator)
    {
        //Traitement du formulaire
        //NB : je n'ai pas utilisé le participantType, car on veut simplement le champs mot de passe et un render_rest à false
        //n'afficherait pas les champs caché pour le jeton CSRF
        if ($request->isMethod("POST")) {

            $email = $request->request->get('email'); //Récupération de l'email

            //On vérifie que l'email rentré correspond bien à un utilisateur en base
            $participantRepo = $this->getDoctrine()->getRepository(Participant::class);
            $participant = $participantRepo->findOneBy(["email" => $email]);
            if ($participant == null) {
                $this->logger->error("Echec de la demande de réinitialisation du mot de passe - Email incorrect");
                $this->addFlash("danger", "Email incorrect");
                return $this->redirectToRoute("participant_new_pass_request");
            }
            //On génère un token qui permettra d'identifier l'utilisateur ensuite et on le sauvegarde en base
            $token = $tokenGenerator->generateToken();
            $participant->setToken($token);
            try {
                $em->persist($participant);
                $em->flush();
            } catch (PDOException $e) {
                $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
            }
            // On utilise le service Mailer pour l'envoi du mail
            $bodyMail = $mailer->createBodyMail('emails/send_link_pass.html.twig', [
                'token' => $token,
                'firstname' => $participant->getPrenom()
            ]);
            $mailer->sendMessage($email, 'Renouvellement du mot de passe', $bodyMail);

            $this->addFlash("success", "Un email vous a été envoyé");
            return $this->redirectToRoute("login");
        }
        //Affichage du formulaire
        return $this->render('participant/new_pass_request.html.twig');
    }

    /**
     * Route de réinitialisation du mot de passe
     * @Route ("/login/updatePassword/{token}", name="participant_update_password")
     * @param Participant $participant
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function updatePassword(Participant $participant, Request $request, EntityManagerInterface $em)
    {
        //On réutilise le formulaire de modification du profil utilisateur (mais en ne gardant juste le champs pass)
        $formPass = $this->createForm(ParticipantType::class, $participant);
        $formPass->remove('pseudo')
            ->remove('nom')
            ->remove('prenom')
            ->remove('telephone')
            ->remove('email')
            ->remove('campus')
            ->remove('photo');

        //Hydratation avec les données du formulaire
        $formPass->handleRequest($request);

        //Traitement du formulaire
        if ($formPass->isSubmitted() && $formPass->isValid()) {

            //On hashe le mot de passe avant de l'insérer en base
            $password = $this->encoder->encodePassword($participant, $participant->getPassword());
            $participant->setPassword($password);
            $em->flush();

            $this->addFlash("success", "Votre mot de passe a été réinitialisé");
            return $this->redirectToRoute("login");
        }

        //Affichage du formulaire
        return $this->render('participant/update_password.html.twig', [
            'formPass' => $formPass->createView()
        ]);
    }

    /**
     * @Route ("/participant/details/{id}/{fromPage}/{sortieId}", name="participant_details",  requirements={"id":"\d+"})
     * @param Participant $participant
     * @param Request $request
     * @param string $fromPage : nécessaire pour savoir vers où on redirige quand on clique sur le bouton retour
     * @param $sortieId : nécessaire quand on revient sur le détail d'une sortie
     * @return Response
     */
    public function details(Participant $participant, Request $request, $fromPage, $sortieId)
    {

        $userForm = $this->createForm(ParticipantType::class, $participant);
        $userForm->handleRequest($request); //Hydratation des données

        return $this->render('participant/details.html.twig', [
            "participant" => $participant,
            "userForm" => $userForm->createView(),
            "fromPage" => $fromPage,
            "sortieId" => $sortieId
        ]);

    }

    /**
     * Route permettant la suppression d'un participant par l'administrateur
     * @Route ("/admin/participant/delete", name="participant_delete")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function deleteParticipant(Request $request, EntityManagerInterface $em)
    {
        //Récupération de l'id utilisateur + vérif format
        $userId = $request->request->get('userId') ?? '';
        if (!is_numeric($userId)) {
            throw new BadRequestHttpException("L'id du participant doit être au format numérique");
        }
        //Récupération de l'utilisateur correspondant à l'id
        $userRepo = $this->getDoctrine()->getRepository(Participant::class);
        $userToDelete = $userRepo->find($userId);

        if (empty($userToDelete)) {
            throw $this->createNotFoundException("L'utilisateur à supprimer n'a pas été trouvée en base");
        }
        try {
            $em->remove($userToDelete);
            $em->flush();
            $status = 200;
            $notif = ['title' => 'Suppression réussie', 'content' => "L'utilisateur " . $userToDelete->getPrenom() . ' ' . $userToDelete->getNom() . ' a bien été supprimé'];
        } catch (PDOException $e) {
            $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
            $status = 500;
            $notif = ['title' => 'Erreur', 'content' => "Erreur lors de la suppression. Veuillez vous rapprocher de l'administrateur"];
        }

        //Nécessaire pour l'actualisation de la table une fois user supprimé
        $data = $this->getUsersList();
        $table = $this->renderView('participant/users_table.html.twig',$data);

        //Retour Json avec le statut de la réponse, la table actualisée et la notification (message erreur/succès)
        return new JsonResponse([
            'status' => $status,
            'table' => $table,
            'notif' => $notif,
        ]);
    }

    private function getUsersList(){
        //Récupération de la liste des participants (pour l'interface admin)
        $userRepo = $this->getDoctrine()->getRepository(Participant::class);
        $participants = $userRepo->getUsersExceptHimself($this->getUser());
        $host = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REDIRECT_BASE'];
        return array(
            'participants' => $participants,
            'host' => $host,
        );
    }

    /**
     * Route permettant à l'admin de désactiver un participant
     * @Route ("/admin/participant/disableOrEnable/{id}", name="participant_disable", requirements={"id":"\d+"})
     * @param Participant $participant
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function disableOrEnable(Participant $participant, EntityManagerInterface $em)
    {
        $actifValue = ($participant->getActif() == true) ? false : true;
        try {
            $participant->setActif($actifValue);
            $em->persist($participant);
            $em->flush();
            $status = 200;
            if ($participant->getActif() == true) {
                $notif = ['title' => 'Succès', 'content' => "L'utilisateur " . $participant->getPrenom() . ' ' . $participant->getNom() . ' a bien été activé'];
            } else {
                $notif = ['title' => 'Succès', 'content' => "L'utilisateur " . $participant->getPrenom() . ' ' . $participant->getNom() . ' a bien été désactivé'];
            }
        } catch (PDOException $e) {
            $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
            $status = 500;
            $notif = ['title' => 'Erreur', 'content' => "Erreur lors de la suppression. Veuillez vous rapprocher de l'administrateur"];
        }
        //Nécessaire pour l'actualisation de la table une fois user supprimé
        $data = $this->getUsersList();
        $table = $this->renderView('participant/users_table.html.twig',$data);

        //Retour Json avec le statut de la réponse, la table actualisée et la notification (message erreur/succès)
        return new JsonResponse([
            'status' => $status,
            'table' => $table,
            'notif' => $notif,
        ]);
    }
    /**
     * Lecture d'un fichier csv
     * @param $csv
     * @return mixed
     */
    private function read($csv)
    {
        $file = fopen($csv, 'r');
        while (!feof($file)) {
            $line[] = fgetcsv($file, 1024);
        }
        fclose($file);
        return $line;
    }
    /**
     * Déconnexion
     * @Route ("/logout", name="logout")
     */
    public function logout()
    {
    }
}
