<?php

namespace App\Repository;

use App\Entity\Inscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Inscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method Inscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method Inscription[]    findAll()
 * @method Inscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Inscription::class);
    }

    public function findInscriptionWithRelatedEntities(){

        $qb = $this->createQueryBuilder('i')
            ->join('i.sortie', 'sortie')
            ->join('i.participant', 'participant')
            ->addSelect("participant")
            ->addSelect('sortie')->distinct();
        $query = $qb->getQuery();

        return $query->getResult();
    }
}
