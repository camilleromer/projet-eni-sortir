<?php

namespace App\Repository;

use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Ville|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ville|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ville[]    findAll()
 * @method Ville[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VilleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ville::class);
    }

    /**
     * Recherche une ville en fonction de son nom
     * @param $name
     * @return int|mixed|string
     */
    public function findVillesByName($name)
    {
        $qb = $this->createQueryBuilder("v");
        $qb->andWhere('v.nom LIKE :nom')
            ->setParameter('nom', '%' . $name . '%')
            ->orderBy('v.id', 'ASC');
        $query = $qb->getQuery();
        return $query->getResult();
    }

    /**
     * Recherche si une ville en base existe déjà avec la propriété mise en param
     * @param $villeId
     * @param $propertyToCompare
     * @param $value
     * @return int|mixed|string
     */
    public function findVilleWithSameProperty($propertyToCompare, $value, $villeId)
    {
        $qb = $this->createQueryBuilder("v");
        $qb->andWhere('v.' . $propertyToCompare . '= :nom');

        if ($villeId != '') {
            $qb->andWhere('v.id <> :id');
            $qb->setParameter('id', $villeId);
        }
        $qb->setParameter('nom', $value)
            ->orderBy('v.id', 'ASC');

        $query = $qb->getQuery();
        return $query->getResult();
    }
}
