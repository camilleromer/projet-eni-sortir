<?php

namespace App\Repository;

use App\Entity\Campus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Campus|null find($id, $lockMode = null, $lockVersion = null)
 * @method Campus|null findOneBy(array $criteria, array $orderBy = null)
 * @method Campus[]    findAll()
 * @method Campus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CampusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Campus::class);
    }

    /**
     * Recherche un campus en fonction de son nom
     * @param $name
     * @return int|mixed|string
     */
    public function findCampusByName($name)
    {
        $qb = $this->createQueryBuilder("c");
        $qb->andWhere('c.nom LIKE :nom')
            ->setParameter('nom', '%' . $name . '%')
            ->orderBy('c.id', 'ASC');
        $query = $qb->getQuery();
        return $query->getResult();
    }

    /**
     * Recherche d'un campus possédant les mêmes propriétés qu'en param
     * (Permet la recherche d'un objet existant en base)
     * @param $propertyToCompare
     * @param $value
     * @param $campusId
     * @return int|mixed|string
     */
    public function findCampusWithSameProperty($propertyToCompare, $value, $campusId)
    {
        $qb = $this->createQueryBuilder("c");
        $qb->andWhere('c.' . $propertyToCompare . '= :property');

        if ($campusId != '') {
            $qb->andWhere('c.id  <> :id');
            $qb->setParameter('id', $campusId);
        }
        $qb->setParameter('property', $value)
            ->orderBy('c.id', 'ASC');
        $query = $qb->getQuery();
        return $query->getResult();
    }
}
