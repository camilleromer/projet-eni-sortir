<?php

namespace App\Repository;

use App\Entity\Participant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Participant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Participant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Participant[]    findAll()
 * @method Participant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParticipantRepository extends ServiceEntityRepository implements UserLoaderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Participant::class);
    }
    /**
     * Récupération des sorties avec ses entités jointes
     */
    public function findUsersWithRelatedEntities()
    {
        $qb = $this->createQueryBuilder("p")
            ->join("p.campus", "campus")->addSelect('campus');
        $query = $qb->getQuery();
        return $query->getResult();
    }

    /**
     * Récupération des utilisateurs autre que celui mis en paramètre
     * @param $user
     * @return int|mixed|string
     */
    public function getUsersExceptHimself($user)
    {
        $qb = $this->createQueryBuilder("p")
            ->join("p.campus", "campus")->addSelect('campus')
            ->andWhere('p.id != :userId')
            ->setParameter('userId', $user->getId())
            ->orderBy('p.id', 'ASC');
        $query = $qb->getQuery();
        return $query->getResult();
    }

    /**
     * Permet de se connecter et par le pseudo et par l'email
     * @param string $username
     * @return int|mixed|string|UserInterface|null
     * @throws NonUniqueResultException
     */
    public function loadUserByUsername($username)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->where('u.pseudo = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username);
        return $qb->getQuery()->getOneOrNullResult();
    }
}
