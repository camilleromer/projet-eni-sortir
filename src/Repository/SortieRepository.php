<?php

namespace App\Repository;

use App\Entity\Inscription;
use App\Entity\Sortie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use function Doctrine\ORM\QueryBuilder;

/**
 * @method Sortie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sortie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sortie[]    findAll()
 * @method Sortie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SortieRepository extends ServiceEntityRepository
{
    private $inscRepo;

    public function __construct(ManagerRegistry $registry, InscriptionRepository $inscRepo)
    {
        parent::__construct($registry, Sortie::class);
        $this->inscRepo = $inscRepo;

    }

    /**
     * Récupération des sorties avec ses entités jointes
     */
    public function findSortiesWithRelatedEntities()
    {
        $qb = $this->createQueryBuilder("s")
            ->join("s.etat", "etat")->addSelect('etat');
        $qb->join("s.organisateur", "orga")->addSelect("orga");

        $query = $qb->getQuery();
        return $query->getResult();
    }

    /**
     * Récupération des sorties en fonction des filtres
     * @param $filters
     * @param $user
     * @return int|mixed|string
     */
    public function findSortiesWithFilters($filters, $user)
    {
        //On crée la requête en fonction des filtres
        $qb = $this->createQueryBuilder("s");
        $qb->join("s.etat", "etat")->addSelect('etat');
        $qb->join("s.organisateur", "orga")->addSelect("orga");
        $qb->join('s.lieu', 'lieu')->addSelect('lieu');

        //NB : si fais un simple join ici, va me chercher seulement les sorties qui sont associées à une inscription (La FK est ici du côté de inscription pas de sortie°
        //Or je veux tout récupérer, joindre me permet juste d'avoir les infos des inscriptions liées pas de filtrer(c'est après que j'ai les filtres)
        $qb->leftJoin("s.inscriptions", "inscriptions")->addSelect("inscriptions");
       $filters['date_fin'] = ($filters['date_fin'] != "") ? $filters['date_fin']. " 23:59:59" : "";

        if ($filters['date_debut'] != "" && $filters['date_fin'] == "") $this->completeQuery($qb, 'dateDebut', $filters['date_debut'], '>= ');
        if ($filters['date_fin'] != "" && $filters['date_debut'] == "") $this->completeQuery($qb, 'dateDebut', $filters['date_fin'], '<= ');
        if ($filters['date_debut'] != "" && $filters['date_fin']!=""){
            $qb->andWhere("s.dateDebut BETWEEN '".$filters['date_debut']."' AND '".$filters['date_fin']."'");
        }

        if ($filters['campus'] != null) $this->completeQuery($qb, 'campus', $filters['campus']);
        if ($filters['isOrganizer']) $this->completeQuery($qb, 'organisateur', $user);
        if ($filters['nom'] != null) $this->completeQuery($qb, 'nom', '%' . $filters['nom'] . '%', 'LIKE');

        if (!$filters['passedOutings']) $this->completeQuery($qb, 'dateDebut', date('Y-m-d'), '>= ');

        if ($filters['isRegistered'] && !$filters['isNotRegistered']) {
            $qb->andWhere('inscriptions.participant = :user');
            $qb->setParameter('user', $user);
        }
        if ($filters['isNotRegistered'] && !$filters['isRegistered']) {
            $dql = 'SELECT sortie FROM App\Entity\Sortie sortie JOIN App\Entity\Inscription i WHERE i.sortie = sortie.id AND  i.participant = :user';
            $qb->andWhere($qb->expr()->notIn('s',$dql))->setParameter('user', $user)->distinct();
        }

        //Les sorties qui n'ont pas encore été publiées (donc en état créée) ne sont visible que par leur organisateur
        $dql = 'SELECT sortie FROM App\Entity\Sortie sortie JOIN App\Entity\Etat e WHERE sortie.etat = e.id AND e.libelle = :etat AND sortie.organisateur <> :user';
        $qb->andWhere($qb->expr()->notIn('s',$dql))->setParameter('user', $user)->setParameter('etat', 'Créée')->distinct();

        $qb->orderBy('s.dateDebut', 'ASC');
        $query = $qb->getQuery();

        //On ne renvoie que les sorties qui datent de moins d'un mois (archivage des sorties)
        $allresults = $query->getResult();
        $finalResults = [];
        foreach ($allresults as $result) {
            $datePlusOneMonth = strtotime($result->getDateDebut()->format('Y-m-d H:i:s')) + 2628000;
            if(time() < $datePlusOneMonth){
                $finalResults[] = $result;
            }

        }
        return $finalResults;
    }


    /**
     * Permet de former la requête
     * @param $qb
     * @param $field
     * @param $value
     * @param string $operator
     */
    private function completeQuery($qb, $field, $value, $operator = "=")
    {
        $qb->andWhere('s.' . $field . ' ' . $operator . ' :' . $field);
        $qb->setParameter($field, $value);
    }


}
