<?php

namespace App\DataFixtures;

use App\Entity\Participant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use \Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ParticipantFixtures extends Fixture  implements DependentFixtureInterface
{   public const USER_1 = 'USER_1';
    public const USER_2 = 'USER_2';
    public const USER_3 = 'USER_3';
    public const USER_4 = 'USER_4';
    public const USER_5 = 'USER_5';

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // On configure dans quelles langues nous voulons nos données
        $faker = Faker\Factory::create('fr_FR');
        $faker->seed(1); //Pour avoir les mêmes fixtures à chaque initialisation
        $password = $this->encoder->encodePassword(new Participant(), 'root');
        $this->userBuilder("admin", $faker, 'ROLE_ADMIN', $this->getReference(CampusFixtures::RENNES), self::USER_1, $manager,$password, '0613526589');
        $this->userBuilder("user", $faker,'ROLE_USER', $this->getReference(CampusFixtures::RENNES),self::USER_2, $manager,$password, '0299586354');
        $this->userBuilder($faker->userName, $faker, 'ROLE_USER', $this->getReference(CampusFixtures::NIORT), self::USER_3,$manager,$password, '0745829565');
        $this->userBuilder($faker->userName, $faker, 'ROLE_USER',$this->getReference(CampusFixtures::NIORT), self::USER_4, $manager,$password, '0654859275');
        $this->userBuilder($faker->userName, $faker, 'ROLE_USER', $this->getReference(CampusFixtures::SAINT_HERBLAIN), self::USER_5,$manager,$password, '0745258965');

        $manager->flush();
    }

    /**
     * Construction d'un participant
     * @param $pseudo
     * @param $faker
     * @param $role
     * @param $campus
     * @param $constUser
     * @param $manager
     * @param $pass
     * @param $tel
     */
    public function userBuilder($pseudo, $faker, $role, $campus, $constUser, $manager, $pass, $tel){
        $user = new Participant();
        $user->setPseudo($pseudo);
        $user->setNom($faker->lastname);
        $user->setPrenom( $faker->firstname);
        //$user->setTelephone($faker->e164PhoneNumber);
        $user->setTelephone($tel);
        if($role == "ROLE_ADMIN"){
            $user->setEmail('camille.romer@gmail.com');
            $user->setPhoto('admin.png');
        }else{
            $user->setEmail( $faker->email);
        }
        $user->setCampus($campus);
        $user->setActif(true);
        $user->setPassword($pass);
        $user->setRoles(array($role));

        $this->addReference($constUser, $user);
        $manager->persist($user);
    }

    public function getDependencies()
    {
        return array(
            CampusFixtures::class
        );
    }

}
