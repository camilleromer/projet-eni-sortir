<?php


namespace App\DataFixtures;


use App\Entity\Etat;
use App\Entity\Sortie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class SortieFixtures extends Fixture  implements DependentFixtureInterface
{
    //Constantes pour les sorties
    public const SORTIE_1 = 'SORTIE_1';
    public const SORTIE_2 = 'SORTIE_2';
    public const SORTIE_3 = 'SORTIE_3';
    public const SORTIE_4 = 'SORTIE_4';
    public const SORTIE_5 = 'SORTIE_5';
    public const SORTIE_6 = 'SORTIE_6';
    public const SORTIE_7 = 'SORTIE_7';
    public const SORTIE_8 = 'SORTIE_8';
    public const SORTIE_9 = 'SORTIE_9';
    public const SORTIE_10 = 'SORTIE_10';
    public const SORTIE_11 = 'SORTIE_11';
    public const SORTIE_12 = 'SORTIE_12';
    public const SORTIE_13 = 'SORTIE_13';

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        $faker->seed(1); //Pour avoir les mêmes fixtures à chaque initialisation

        //Construction des sorties
        $this->sortieBuilder($faker,"Philo", new \DateTime("2020-09-19 17:30"), new \DateTime("2020-09-17"), ParticipantFixtures::USER_1, EtatFixtures::ETAT_PASSEE, LieuFixtures::LIEU_5,CampusFixtures::RENNES, $manager, self::SORTIE_1, 'Cours gratuit de philo', 180, 5);
        $this->sortieBuilder($faker, "Origami", new \DateTime("2020-11-15 18:00"), new \DateTime("2020-11-14"),ParticipantFixtures::USER_1, EtatFixtures::ETAT_OUVERTE, LieuFixtures::LIEU_3,CampusFixtures::RENNES, $manager, self::SORTIE_2, "Initiation à l'origami", 120, 10);
        $this->sortieBuilder($faker, "Danse", new \DateTime("2020-09-19 19:00"), new \DateTime("2020-08-18"),ParticipantFixtures::USER_1, EtatFixtures::ETAT_PASSEE, LieuFixtures::LIEU_3, CampusFixtures::RENNES, $manager, self::SORTIE_3, "Cours d'essai à la danse contemporaine", 90,8);
        $this->sortieBuilder($faker, "Perles", new \DateTime("2020-11-20 18:15"), new \DateTime("2020-11-18"),ParticipantFixtures::USER_2, EtatFixtures::ETAT_CREEE, LieuFixtures::LIEU_3, CampusFixtures::RENNES, $manager, self::SORTIE_4, 'Atelier perle pour débutants', 120,5);
        $this->sortieBuilder($faker, "Concert métal", new \DateTime("2020-12-12 23:00"), new \DateTime("2020-05-12"),ParticipantFixtures::USER_2, EtatFixtures::ETAT_CLOTUREE, LieuFixtures::LIEU_1, CampusFixtures::SAINT_HERBLAIN, $manager, self::SORTIE_5, "Concert du groupe Metallica", 180,7);
        $this->sortieBuilder($faker, "Jardinage", new \DateTime("2020-11-19 17:15"), new \DateTime("2020-11-18"),ParticipantFixtures::USER_2, EtatFixtures::ETAT_OUVERTE, LieuFixtures::LIEU_4, CampusFixtures::RENNES, $manager, self::SORTIE_6, 'Découverte des bases de jardinage', 120,10);
        $this->sortieBuilder($faker, "Cinéma", new \DateTime("2020-10-19 19:30"), new \DateTime("2020-10-17"),ParticipantFixtures::USER_2, EtatFixtures::ETAT_OUVERTE, LieuFixtures::LIEU_2, CampusFixtures::RENNES, $manager, self::SORTIE_7, "Film à déterminer en groupe, plusieurs sessions possibles", 150,8);
        $this->sortieBuilder($faker, "Pâte à sel", new \DateTime("2020-11-05 18:00"), new \DateTime("2020-11-01"),ParticipantFixtures::USER_2, EtatFixtures::ETAT_CREEE, LieuFixtures::LIEU_3, CampusFixtures::RENNES, $manager, self::SORTIE_8, "Atelier avancé de pâte à sel", 60,7);
        $this->sortieBuilder($faker,"Jeux de société", new \DateTime("2020-11-20 20:30"), new \DateTime("2020-11-19"), ParticipantFixtures::USER_2, EtatFixtures::ETAT_CLOTUREE, LieuFixtures::LIEU_3, CampusFixtures::RENNES, $manager, self::SORTIE_9, 'Avis aux amateurs de jeux de société en groupe !', 180, 3);
        $this->sortieBuilder($faker,"Soirée disco", new \DateTime("2020-12-04 20:00"), new \DateTime("2020-11-04"), ParticipantFixtures::USER_1, EtatFixtures::ETAT_CREEE, LieuFixtures::LIEU_1,CampusFixtures::SAINT_HERBLAIN, $manager, self::SORTIE_10, 'Tout le monde est invité, tout niveau accepté, avant tout pour le plaisir !', 125,8);
        $this->sortieBuilder($faker,"Foot", new \DateTime("2020-11-04 20:00"), new \DateTime("2020-11-02"), ParticipantFixtures::USER_1, EtatFixtures::ETAT_CREEE, LieuFixtures::LIEU_6,CampusFixtures::RENNES, $manager, self::SORTIE_11, 'Match amical', 120);
        $this->sortieBuilder($faker,"Atelier artistique", new \DateTime("2020-10-16 09:00"), new \DateTime("2020-10-09"), ParticipantFixtures::USER_1, EtatFixtures::ETAT_CLOTUREE, LieuFixtures::LIEU_3,CampusFixtures::RENNES, $manager, self::SORTIE_12, 'Initiation au dessin et à la peinture', 240,5);
        $this->sortieBuilder($faker,"Soirée lecture", new \DateTime("2020-10-22 18:00"), new \DateTime("2020-10-20"), ParticipantFixtures::USER_2, EtatFixtures::ETAT_CREEE, LieuFixtures::LIEU_3,CampusFixtures::RENNES, $manager, self::SORTIE_13, 'Echange sur différentes lectures', 180,6);

        $manager->flush();
    }
    public function sortieBuilder($faker, $nom, $dateDebut, $dateCLoture, $orga, $etat, $lieu, $campus, $manager, $constSortie, $description, $duree, $nbPlaces='')
    {
        $sortie = new Sortie();
        $sortie->setNom($nom);
        $sortie->setDuree($duree);
        $sortie->setDateDebut($dateDebut);
        $sortie->setDateCloture($dateCLoture);
        $nbPlaces = ($nbPlaces == '') ? $faker->numberBetween(5,10) : $nbPlaces;
        $sortie->setNbInscriptionsMax($nbPlaces);
        $sortie->setDescription($description);
        $sortie->setOrganisateur($this->getReference($orga));
        $sortie->setEtat($this->getReference($etat));
        $sortie->setLieu($this->getReference($lieu));
        $sortie->setCampus($this->getReference($campus));
        $this->addReference($constSortie, $sortie);
        $manager->persist($sortie);
    }
    public function getDependencies()
    {
        return array(
            LieuFixtures::class,
            ParticipantFixtures::class,
            EtatFixtures::class,
        );
    }

}