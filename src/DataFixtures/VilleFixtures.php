<?php


namespace App\DataFixtures;


use App\Entity\Ville;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class VilleFixtures extends Fixture
{
    public const VILLE_1 = 'VILLE_1';
    public const VILLE_2 = 'VILLE_2';
    public const VILLE_3 = 'VILLE_3';
    public const VILLE_4 = 'VILLE_4';

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        $faker->seed(1); //Pour avoir les mêmes fixtures à chaque initialisation

        $this->villeBuilder('Rennes', '35000', self::VILLE_1, $manager);
        $this->villeBuilder('Nante', '44000',self::VILLE_2, $manager);
        $this->villeBuilder('Niort','79000', self::VILLE_3, $manager);
        $this->villeBuilder('Paris','75000', self::VILLE_4, $manager);

        $manager->flush();

    }

    public function villeBuilder($nom, $cp,  $constVille, $manager){
        $ville = new Ville();
        $ville->setNom($nom);
        $ville->setCodepostal($cp);
        $this->addReference($constVille, $ville);
        $manager->persist($ville);
    }

}