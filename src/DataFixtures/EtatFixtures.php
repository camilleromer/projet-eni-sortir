<?php


namespace App\DataFixtures;


use App\Entity\Campus;
use App\Entity\Etat;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class EtatFixtures extends Fixture
{
    public const ETAT_CREEE = 'Créée';
    public const ETAT_OUVERTE = 'Ouverte';
    public const ETAT_CLOTUREE = 'Cloturée';
    public const ETAT_ACTIVITE_EN_COURS = 'En cours';
    public const ETAT_PASSEE = 'Passée';
    public const ETAT_ANNULEE = 'Annulée';

    public function load(ObjectManager $manager)
    {
        $this->etatBuilder(self::ETAT_CREEE, $manager);
        $this->etatBuilder(self::ETAT_OUVERTE, $manager);
        $this->etatBuilder(self::ETAT_CLOTUREE, $manager);
        $this->etatBuilder(self::ETAT_ACTIVITE_EN_COURS, $manager);
        $this->etatBuilder(self::ETAT_PASSEE, $manager);
        $this->etatBuilder(self::ETAT_ANNULEE, $manager);

        $manager->flush();
    }

    public function etatBuilder($libelle, $manager)
    {
        $etat = new Etat();
        $etat->setLibelle($libelle);
        $this->addReference($libelle, $etat);
        $manager->persist($etat);
    }

}