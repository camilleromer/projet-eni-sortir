<?php


namespace App\DataFixtures;


use App\Entity\Lieu;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class LieuFixtures extends Fixture  implements DependentFixtureInterface
{
    public const LIEU_1 = 'LIEU_1';
    public const LIEU_2 = 'LIEU_2';
    public const LIEU_3 = 'LIEU_3';
    public const LIEU_4 = 'LIEU_4';
    public const LIEU_5 = 'LIEU_5';
    public const LIEU_6 = 'LIEU_6';

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        $faker->seed(1); //Pour avoir les mêmes fixtures à chaque initialisation

        $this->lieuBuilder($faker, 'Salle de concert', $this->getReference(VilleFixtures::VILLE_2), self::LIEU_1, $manager);
        $this->lieuBuilder($faker, 'Centre ville', $this->getReference(VilleFixtures::VILLE_1),self::LIEU_2, $manager);
        $this->lieuBuilder($faker, 'Centre de Loisir', $this->getReference(VilleFixtures::VILLE_1),self::LIEU_3, $manager);
        $this->lieuBuilder($faker, 'Parc principal',$this->getReference(VilleFixtures::VILLE_1),self::LIEU_4, $manager);
        $this->lieuBuilder($faker, 'Annexe université', $this->getReference(VilleFixtures::VILLE_1),self::LIEU_5, $manager);
        $this->lieuBuilder($faker, 'Stade Rennais', $this->getReference(VilleFixtures::VILLE_1),self::LIEU_6, $manager);

        $manager->flush();
    }

    public function lieuBuilder($faker, $nomLieu, $ville, $constLieu, $manager)
    {
        $lieu = new Lieu();
       // $lieu->setNom($faker->company);
        $lieu->setNom($nomLieu);
        $lieu->setRue($faker->streetName);
        $lieu->setLatitude( $faker->latitude);
        $lieu->setLongitude( $faker->longitude);
        $lieu->setVille($ville);
        $this->addReference($constLieu, $lieu);
        $manager->persist($lieu);
    }


    public function getDependencies()
    {
        return array(
            VilleFixtures::class
        );
    }
}