<?php


namespace App\DataFixtures;

use App\Entity\Campus;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Faker;


class CampusFixtures extends Fixture
{
    public const RENNES = 'RENNES';
    public const SAINT_HERBLAIN= 'SAINT_HERBLAIN';
    public const NIORT = 'NIORT';

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function load(ObjectManager $manager)
    {

        //Exécution script de reset avant lancement des fixtures
        $connection = $this->em->getConnection();

        try {
            $connection->exec('
                ALTER TABLE etat AUTO_INCREMENT = 1;
                ALTER TABLE ville AUTO_INCREMENT = 1;
                ALTER TABLE lieu AUTO_INCREMENT = 1;
                ALTER TABLE participant AUTO_INCREMENT = 1;
                ALTER TABLE campus AUTO_INCREMENT = 1;
                ALTER TABLE Sortie AUTO_INCREMENT = 1;
                ALTER TABLE inscription AUTO_INCREMENT = 1;
');
        } catch (DBALException $e) {
        }
        $this->campusBuilder('RENNES', self::RENNES, $manager);
        $this->campusBuilder('SAINT HERBLAIN', self::SAINT_HERBLAIN, $manager);
        $this->campusBuilder('NIORT', self::NIORT, $manager);

        $manager->flush();
    }

    public function campusBuilder($name, $const, $manager){
        $campus = new Campus();
        $campus->setNom($name);
        $this->addReference($const, $campus);
        $manager->persist($campus);
    }
}