<?php


namespace App\DataFixtures;


use App\Entity\Inscription;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Faker;

class InscriptionFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {

        $this->inscriptionBuilder(new \DateTime("2020-07-15 15:00"), $this->getReference(ParticipantFixtures::USER_2), $this->getReference(SortieFixtures::SORTIE_1), $manager);
        $this->inscriptionBuilder(new \DateTime("2020-07-13 14:00"), $this->getReference(ParticipantFixtures::USER_4), $this->getReference(SortieFixtures::SORTIE_1), $manager);
        $this->inscriptionBuilder(new \DateTime("2020-07-11 16:00"), $this->getReference(ParticipantFixtures::USER_5), $this->getReference(SortieFixtures::SORTIE_1), $manager);

        $this->inscriptionBuilder(new \DateTime("2020-10-04 17:05"), $this->getReference(ParticipantFixtures::USER_1), $this->getReference(SortieFixtures::SORTIE_2), $manager);
        $this->inscriptionBuilder(new \DateTime("2020-10-05 18:10"), $this->getReference(ParticipantFixtures::USER_2), $this->getReference(SortieFixtures::SORTIE_2), $manager);
        $this->inscriptionBuilder(new \DateTime("2020-10-02 19:15"), $this->getReference(ParticipantFixtures::USER_5), $this->getReference(SortieFixtures::SORTIE_2), $manager);

        $this->inscriptionBuilder(new \DateTime("2020-08-17 20:00"), $this->getReference(ParticipantFixtures::USER_3), $this->getReference(SortieFixtures::SORTIE_3), $manager);
        $this->inscriptionBuilder(new \DateTime("2020-08-18 17:45"), $this->getReference(ParticipantFixtures::USER_4), $this->getReference(SortieFixtures::SORTIE_3), $manager);

        $this->inscriptionBuilder(new \DateTime("2020-11-17 18:10"), $this->getReference(ParticipantFixtures::USER_2), $this->getReference(SortieFixtures::SORTIE_6), $manager);
        $this->inscriptionBuilder(new \DateTime("2020-10-15"), $this->getReference(ParticipantFixtures::USER_2), $this->getReference(SortieFixtures::SORTIE_7), $manager);

        $this->inscriptionBuilder(new \DateTime("2020-11-17 09:25"), $this->getReference(ParticipantFixtures::USER_1), $this->getReference(SortieFixtures::SORTIE_9), $manager);
        $this->inscriptionBuilder(new \DateTime("2020-11-15 10:00"), $this->getReference(ParticipantFixtures::USER_4), $this->getReference(SortieFixtures::SORTIE_9), $manager);
        $this->inscriptionBuilder(new \DateTime("2020-11-20 11:20"), $this->getReference(ParticipantFixtures::USER_5), $this->getReference(SortieFixtures::SORTIE_9), $manager);

        $this->inscriptionBuilder(new \DateTime("2020-10-18 10:00"), $this->getReference(ParticipantFixtures::USER_1), $this->getReference(SortieFixtures::SORTIE_12), $manager);
        $this->inscriptionBuilder(new \DateTime("2020-10-27 11:20"), $this->getReference(ParticipantFixtures::USER_2), $this->getReference(SortieFixtures::SORTIE_12), $manager);

        $manager->flush();
    }

    private function inscriptionBuilder($dateInscription, $participant, $sortie, $manager){

        $inscription = new Inscription();
        $inscription->setDateInscription($dateInscription);
        $inscription->setParticipant($participant);
        $inscription->setSortie($sortie);

        $manager->persist($inscription);
    }

    public function getDependencies()
    {
        return array(
            SortieFixtures::class,
            ParticipantFixtures::class,
        );
    }
}