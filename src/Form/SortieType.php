<?php

namespace App\Form;

use App\Entity\Campus;
use App\Entity\Lieu;
use App\Entity\Sortie;
use App\Entity\Ville;
use DateTime;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class SortieType extends AbstractType
{

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('nom', TextType::class, [
                'label' => 'Nom de la sortie',
                'trim' => true
            ])
            ->add('date_debut', DateTimeType::class, [
                'label' => 'Date et heure du début',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy H:mm',
                'attr' => [
                    'class' => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datetimepicker',
                    'html5' => false,
                ],
            ])
            ->add('date_cloture', DateType::class, [
                'label' => "Date limite d'inscription",
                'widget' => 'single_text',

            ])
            ->add('nb_inscriptions_max', IntegerType::class, [
                'label' => 'Nombre de places',
                'attr' => [
                    'min' => 1 //Il faut au moins une personne pour l'activité
                ]
            ])
            ->add('duree', IntegerType::class, [
                'label' => 'Durée',
                'attr' => [
                    'min' => 15 //On considère qu'une activité dure au moins 15 minutes
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description et infos',
                'trim' => true
            ])
            ->add("campus", EntityType::class, [
                "choice_label" => "nom", //On indique à quel champ de la table va correspondre le label,
                "class" => Campus::class,
                'required' => true,
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('c')->orderBy('c.id');
                }
            ])
            ->add("ville", EntityType::class, [
                "choice_label" => "nom", //On indique à quel champ de la table va correspondre le label,
                "class" => Ville::class,
                'required' => true,
                'mapped' => false
            ])
            ->add("lieu", EntityType::class, [
                "placeholder" => 'Choisir un lieu',
                "choice_label" => "nom", //On indique à quel champ de la table va correspondre le label,
                "class" => Lieu::class,
                'required' => true
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sortie::class,
        ]);
    }
}
