<?php

namespace App\Form;

use App\Entity\Lieu;
use App\Entity\Ville;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LieuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'label' => 'Nom',
                'trim' => true
            ])
            ->add('rue',TextType::class, [
                'label' => 'Rue',
                'required' => false,
                'trim' => true
            ])
            ->add('latitude',
                TextType::class, [
                    'label' => 'Latitude',
                    'required' => false,
                    'trim' => true
                ])
            ->add('longitude',
                TextType::class, [
                    'label' => 'Longitude',
                    'required' => false,
                    'trim' => true
                ])
            ->add("ville", EntityType::class,[
                "choice_label" => "nom",
                "class" => Ville::class,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lieu::class,
        ]);
    }
}
