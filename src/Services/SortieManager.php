<?php


namespace App\Services;


use App\Controller\SortieController;
use App\Entity\Campus;
use App\Entity\Etat;
use App\Entity\Sortie;
use Doctrine\DBAL\Driver\PDOException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * Ensemble des services liés aux sortie (vérif des inputs...)
 * Class SortieManager
 * @package App\Services
 */
class SortieManager{
    private $session;
    private $security;
    private $logger;
    private $em;
    private $sortieController;

    public function __construct(SessionInterface $session, Security $security, LoggerInterface $logger, EntityManagerInterface $em, SortieController $sortieController)
    {
        $this->session = $session;
        $this->security = $security;
        $this->logger = $logger;
        $this->em = $em;
        $this->sortieController = $sortieController;
    }
    /**
     * Traitement des actions accessibles via les pages d'ajout/modif d'une sortie
     * @param $fromPage
     * @param $sortie
     * @param $em
     * @param $request
     * @param $sortieForm
     * @return array[]
     */
    public function manageSortie($fromPage, $sortie, $em, $request, $sortieForm)
    {
        //On récupère l'action (Enregistrer / publier / supprimer)
        $action = $request->request->get('btnSubmit');
        //On redirigera sur la liste des sorties par défaut
        $redirectTo = ['name' => 'sortie_list', 'params' => null];

        //Vérification des dates
        $today = time();
        $dateClotureWithSeconds = $sortieForm->getData()->getDateCloture()->format('Y-m-d') . " 23:59:59"; //On donne jusqu'à la fin de la journée
        $dateCloture = strtotime($dateClotureWithSeconds);
        $dateDebut = strtotime($sortieForm->getData()->getDateDebut()->format('Y-m-d H:i:s'));

        $errorMessage = '';
        //La date de début ne doit pas être inférieure à la date du jour
        if ($dateDebut < $today) {
            $errorMessage = "La date de la sortie ne peut pas être inférieure à la date actuelle";
            //La date de clôture doit se trouver entre la date du jour et la date de début
        } else if ($dateCloture > $dateDebut || $dateCloture < $today) {
            $errorMessage = "La date de clôture doit se situer entre la date du jour et la date de début de la sortie";
        }
        if ($errorMessage != '') {
            $this->session->getFlashBag()->add('danger', $errorMessage);
            $routeName = ($fromPage == 'update') ? 'sortie_update' : 'sortie_add';
            $routeParams = ($fromPage == 'update') ? ['id' => $sortie->getId()] : null;
            $redirectTo = ['name' => $routeName, 'params' => $routeParams];
            return $redirectTo;
        }
        //Traitement en fonction de l'action à effectuer
        switch ($action) {
            //BOUTON D'ENREGISTREMENT
            case 'save':
                if ($fromPage == 'create') {
                    $sortie->setOrganisateur($this->security->getUser());
                    $sortie->setEtat($this->getStateObject('Créée'));
                }
                //Enregistrement des résultats
                try {
                    $em->persist($sortie);
                    $em->flush();
                    $message = ($fromPage == 'create') ? 'La sortie a bien été enregistrée, pensez à la publier' : 'La sortie a bien été modifiée';
                    $this->session->getFlashBag()->add('success', $message);
                } catch (PDOException $e) {
                    $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
                }
                break;

            //BOUTON DE PUBLICATION
            case 'publish':
                //Enregistrement d'abord des résultats
                if ($fromPage == 'create') {
                    $sortie->setOrganisateur($this->security->getUser());
                    $sortie->setEtat($this->getStateObject('Créée'));
                }
                try {
                    $em->persist($sortie);
                    $em->flush();
                    //Puis redirection vers la route de publication
                    $redirectTo = ['name' => 'sortie_publish', 'params' => ['id' => $sortie->getId()]];
                } catch (PDOException $e) {
                    $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
                }
                break;

            //BOUTON DE SUPPRESSION (Suppression en base ici contrairement à l'annulation)
            case 'delete':
                //On récupère l'id dans le champ caché (suppression en POST car plus sécure)
                $sortieId = $request->request->get('sortieId') ?? null;
                //On vérifie le format de l'id
                if (!is_numeric($sortieId)) {
                    throw new BadRequestHttpException("L'id de la sortie doit être au format numérique");
                }
                //On récupère la sortie correspondante en base (on vérifie son existence)
                $sortieRepo = $this->em->getRepository(Sortie::class);
                $sortieToDelete = $sortieRepo->find($sortieId);
                if (empty($sortieToDelete)) {
                    throw $this->sortieController->createNotFoundException("La sortie a supprimer n'a pas été trouvée en base");
                }
                try {
                    $em->remove($sortieToDelete);
                    $em->flush();
                    $this->session->getFlashBag()->add('success', 'La sortie ' . $sortieToDelete->getNom() . ' a bien été supprimée');
                } catch (PDOException $e) {
                    $this->logger->error('PDOException : ' . $e->getCode() . " : " . $e->getMessage());
                }
                break;
        }
        //On renvoit le nom et les paramètres de la route vers lequel on redirigera l'utilisateur
        return $redirectTo;
    }


    /**
     * Met à jour les états des sorties en fonction de la date actuelle
     * @param $sorties
     * @param $em
     */
    private function updateSortiesStates($sorties, $em)
    {
        $today = time();

        //Pour chaque sortie on compare la date actuelle avec les dates liées à la sortie
        foreach ($sorties as $sortie) {

            $newEtat = null;
            $dateClotureWithSeconds = $sortie->getDateCloture()->format('Y-m-d') . "23:59:59";
            $dateCloture = strtotime($dateClotureWithSeconds);
            //$dateCloture = strtotime($sortie->getDateCloture()->format('Y-m-d'));
            $dateDebut = strtotime($sortie->getDateDebut()->format('Y-m-d H:i:s'));
            $dateFin = $dateDebut + ($sortie->getDuree() * 60);

            $nbInscrits = count($sortie->getInscriptions()); //On récupère le nombre d'inscrits
            //Si la sortie est annulée on la laisse comme ça on ne met pas à jour (sinon repasse à son état initial)
            if ($sortie->getEtat()->getId() != 6) {
                try {
                    //Si date du jour entre la date de début et de fin de sortie => EN COURS
                    if ($today > $dateDebut && $today < $dateFin) {
                        $newEtat = $this->getStateObject('En cours');
                    }
                    //Si on a dépassé la date de fin => PASSEE
                    if ($today > $dateFin) {
                        $newEtat = $this->getStateObject('Passée');
                    }
                    //Si on a dépassé la date limite d'inscription et que la sortie n'a pas commence => CLOTUREE
                    //Ou si le nombre max de place a été atteint
                    if ($today > $dateCloture && $today < $dateDebut || $nbInscrits == $sortie->getNbInscriptionsMax()) {
                        $newEtat = $this->getStateObject('Cloturée');
                    }

                    //Si jamais une personne s'est désinscrite entre temps (et que la date n'est pas dépassée on repasse de l'état clôturé à ouvert)
                    if ($sortie->getEtat()->getId() == 3 && $nbInscrits < $sortie->getNbInscriptionsMax() && $today < $dateCloture) {
                        $newEtat = $this->getStateObject('Ouverte');
                    }
                } catch (Exception $e) {
                }
                //On met à jour l'état en base
                if ($newEtat != null) {

                    $sortie->setEtat($newEtat);
                    $em->persist($sortie);
                    $em->flush();
                }
            }
        }
    }

    /**
     * Récupération de l'ensemble des sorties en fonction des filtres sélectionnés
     * @param $postData
     * @param $em
     * @return array
     */
    public function getSortiesList($postData, $em)
    {
        //On construit le tableau de filtres avec les données postées
        $filters['campus'] = $postData['campus'] ?? $this->security->getUser()->getCampus()->getId(); //Campus du participant par défaut
        $filters['nom'] = $postData['nom'] ?? null;
        $filters['date_debut'] = $postData['date_debut'] ?? "";
        $filters['date_fin'] = $postData['date_fin'] ?? "";
        $filters['isOrganizer'] = (empty($postData)) ? true : ($postData['isOrganizer']) ?? false;
        $filters['isRegistered'] = (empty($postData)) ? true : ($postData['isRegistered']) ?? false;
        $filters['isNotRegistered'] = (empty($postData)) ? true : ($postData['isNotRegistered']) ?? false;
        $filters['passedOutings'] = (empty($postData)) ? false : ($postData['passedOutings']) ?? false;

        //Récupération de la liste des campus pour les filtres
        $campusRepo =  $this->em->getRepository(Campus::class);
        $campus = $campusRepo->findAll();

        //Récupération des sorties en fonction des filtres
        $sortieRepo = $this->em->getRepository(Sortie::class);
        $sorties = $sortieRepo->findSortiesWithFilters($filters, $this->security->getUser());

        //Mise à jour des états des sorties (en cours, passée, clôturée)
        $this->updateSortiesStates($sorties, $em);
        $host = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REDIRECT_BASE'];

        return array(
            'listCampus' => $campus,
            'sorties' => $sorties,
            'selectedValues' => $filters,
            'host' => $host
        );
    }
    /**
     * Renvoie l'objet état à partir de son libelle
     * @param $libelle
     * @return Etat|object|null
     */
    private function getStateObject($libelle)
    {
        $etatRepo = $this->em->getRepository(Etat::class);
        $etat = $etatRepo->findOneBy(['libelle' => $libelle]);
        if (empty($etat)) {
            throw $this->sortieController->createNotFoundException("La sortie a supprimer n'a pas été trouvée en base");
        }
        return $etat;
    }

}