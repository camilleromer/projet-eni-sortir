<?php

namespace App\Services;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class Mailer
 */
class Mailer
{
    private $engine;
    private $mailer;

    public function __construct(\Swift_Mailer $mailer, Environment $engine)
    {
        $this->engine = $engine;
        $this->mailer = $mailer;
    }

    /**
     * Envoi d'un message en utilisant SwiftMailer
     * @param $to
     * @param $subject
     * @param $body
     * @param null $attachement
     */
    public function sendMessage($to, $subject, $body, $attachement = null)
    {
        $mail = (new \Swift_Message($subject))
            ->setFrom("camille.romer@gmail.com")
            ->setTo($to)
            ->setSubject($subject)
            ->setBody($body)
            ->setContentType('text/html');

        $this->mailer->send($mail);
    }

    /**
     * Création du corps du mail pour l'utilisation d'un template
     * @param $view
     * @param array $parameters
     * @return string
     */
    public function createBodyMail($view, array $parameters)
    {
        try {
            return $this->engine->render($view, $parameters);
        } catch (LoaderError $e) {
        } catch (RuntimeError $e) {
        } catch (SyntaxError $e) {
        }
    }
}