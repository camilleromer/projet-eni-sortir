<?php

namespace App\Tests\Controller;

use App\Controller\CampusController;
use App\Entity\Campus;
use App\Tests\FixtureAwareTestCase;
use PHPUnit\Framework\TestCase;

class CampusControllerTest extends FixtureAwareTestCase
{
    private $entityManager;
    private $campusRepository;

    protected function setUp()
    {
        parent::setUp();
        $kernel = static::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->campusRepository = $this->entityManager->getRepository(Campus::class);
    }

    public function testCreate(){

        //On créé un nouvel objet Ville
        $campusExpected = new Campus();
        $campusExpected->setNom('PARIS');
        //On l'insère
        $this->entityManager->persist($campusExpected);
        $this->entityManager->flush();
        //On récupère la nouvelle ligne insérée
        $lastCampus = $this->campusRepository->find($this->getLastId());
        //On compare les deux
        $this->assertEquals($campusExpected->getNom(),$lastCampus->getNom());

    }
    public function testUpdate(){
        //On créé un nouvel objet Ville
        $campusExpected = new Campus();
        $campusExpected->setNom('Test');
        //On l'insère
        $this->entityManager->persist($campusExpected);
        $this->entityManager->flush();

        //On récupère la nouvelle ligne insérée
        $lastCampus = $this->campusRepository->find($this->getLastId());
        $lastCampus->setNom('TestAfterUpdate');
        $this->entityManager->persist($lastCampus);
        $this->entityManager->flush();

        $lastCampusAfterUpdate = $this->campusRepository->find($this->getLastId());

        //On compare les deux
        $this->assertEquals($campusExpected->getNom(),$lastCampusAfterUpdate->getNom());
    }
    public function testDelete(){
        $campus =  $this->campusRepository->findAll();
        $sizeBeforeDelete = count($campus);
        $campusToRemove = $this->campusRepository->find($this->getLastId());
        $this->entityManager->remove($campusToRemove);
        $this->entityManager->flush();
        $campusAfterDelete =  $this->campusRepository->findAll();
        $sizeAfterDelete = count($campusAfterDelete);
        $this->assertEquals($sizeBeforeDelete, $sizeAfterDelete + 1);
    }

    private function getLastId(){
        $lastCampus = $this->campusRepository->findBy([], ['id' => 'DESC'], 1, 0);
        return $lastCampus[0]->getId();
    }
}
