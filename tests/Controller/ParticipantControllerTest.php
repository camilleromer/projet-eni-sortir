<?php

namespace App\Tests\Controller;

use App\Controller\ParticipantController;
use App\Entity\Campus;
use App\Entity\Participant;
use App\Repository\ParticipantRepository;
use App\Tests\FixtureAwareTestCase;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Classe de tests pour les actions liées aux participants
 * Class ParticipantControllerTest
 * @package App\Tests\Controller
 */
class ParticipantControllerTest extends FixtureAwareTestCase
{

    private $encoder;


    protected function setUp(): void
    {
        parent::setUp();
        $kernel = static::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->participantRepository = $this->entityManager->getRepository(Participant::class);
    }

    /*************************************
     * Tests unitaires ajout/update/delete
     **************************************/

    function testCreate(){
            $this->assertTrue(true);
    }

    //Test de la fonction d'update
    function testUpdate()
    {
        $this->assertTrue(true);

    }

    /**
     * Construction d'un participant
     * @param $data
     * @param $em
     * @return Participant
     */
    public function userBuilder($data, $em)
    {
        $encoder = $this->createMock(UserPasswordEncoderInterface::class);
        $password = $encoder->encodePassword(new Participant(), 'root');

        $campus = new Campus();
        $campus->setNom('RENNES');
        $em->persist($campus);
        $em->flush();

        $user = new Participant();
        $user->setPseudo('pseudo');
        $user->setNom('nom');
        $user->setPrenom('prenom');
        $user->setTelephone('tel');
        $user->setEmail('camille.romer@gmail.com');
        $user->setCampus($campus);
        $user->setActif(true);
        $user->setPassword($password);
        $user->setRoles(array('ROLE_USER'));
        return $user;
    }
}
