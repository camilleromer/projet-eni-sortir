<?php

namespace App\Tests\Controller;

use App\DataFixtures\CampusFixtures;
use App\DataFixtures\EtatFixtures;
use App\DataFixtures\InscriptionFixtures;
use App\DataFixtures\LieuFixtures;
use App\DataFixtures\ParticipantFixtures;
use App\DataFixtures\SortieFixtures;
use App\DataFixtures\VilleFixtures;
use App\Entity\Ville;
use App\Tests\FixtureAwareTestCase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class VilleControllerTest extends FixtureAwareTestCase
{
    private $entityManager;
    private $villeRepository;

    protected function setUp()
    {
        parent::setUp();
        $kernel = static::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->villeRepository = $this->entityManager->getRepository(Ville::class);

        //On réinitialise la base en exécutant les fixtures entre chaque test
       /* $this->addFixture(new CampusFixtures($this->entityManager));
        $this->addFixture(new EtatFixtures());
        $this->addFixture(new VilleFixtures());
        $this->addFixture(new LieuFixtures());
        $this->addFixture(new ParticipantFixtures(  $this->entityManager));
        $this->addFixture(new SortieFixtures());
        $this->addFixture(new InscriptionFixtures());
        $this->executeFixtures();*/
    }

    public function testCreate(){

        //On créé un nouvel objet Ville
        $villeExpected = new Ville();
        $villeExpected->setNom('Brest');
        $villeExpected->setCodepostal('29000');

        //On l'insère
        $this->entityManager->persist($villeExpected);
        $this->entityManager->flush();

        //On récupère la nouvelle ligne insérée
        $lastVille = $this->villeRepository->find($this->getLastId());

        //On compare les deux
        $this->assertEquals($villeExpected->getNom(),$lastVille->getNom());

    }
    public function testUpdate(){
        //On créé un nouvel objet Ville
        $villeExpected = new Ville();
        $villeExpected->setNom('Test');
        $villeExpected->setCodepostal('57000');

        //On l'insère
        $this->entityManager->persist($villeExpected);
        $this->entityManager->flush();

        //On récupère la nouvelle ligne insérée
        $lastVille = $this->villeRepository->find($this->getLastId());
        $lastVille->setNom('Metz');
        $this->entityManager->persist($lastVille);
        $this->entityManager->flush();

        $lastVilleAfterUpdate = $this->villeRepository->find($this->getLastId());

        //On compare les deux
        $this->assertEquals($villeExpected->getNom(),$lastVilleAfterUpdate->getNom());
    }
    public function testDelete(){
        $villes =  $this->villeRepository->findAll();
        $sizeBeforeDelete = count($villes);
        $villeToRemove = $this->villeRepository->find($this->getLastId());
        $this->entityManager->remove($villeToRemove);
        $this->entityManager->flush();
        $villesAfterDelete =  $this->villeRepository->findAll();
        $sizeAfterDelete = count($villesAfterDelete);
        $this->assertEquals($sizeBeforeDelete, $sizeAfterDelete + 1);
    }

    private function getLastId(){
        $lastVille = $this->villeRepository->findBy([], ['id' => 'DESC'], 1, 0);
        return $lastVille[0]->getId();
    }

}
