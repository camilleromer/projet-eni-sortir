<?php

namespace App\Tests\Controller;

use App\Controller\LieuController;
use App\Entity\Lieu;
use App\Entity\Ville;
use App\Tests\FixtureAwareTestCase;
use PHPUnit\Framework\TestCase;

class LieuControllerTest extends FixtureAwareTestCase
{
    private $entityManager;
    private $lieuRepository;

    protected function setUp()
    {
        parent::setUp();
        $kernel = static::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->lieuRepository = $this->entityManager->getRepository(Lieu::class);
    }

    public function testCreate()
    {
        $lieuExpected = $this->lieuBuilder(null,  $this->entityManager);
        $this->entityManager->persist($lieuExpected);
        $this->entityManager->flush();
        $lastLieu = $this->lieuRepository->find($this->getLastId());
        $this->assertEquals($lieuExpected->getNom(), $lastLieu->getNom());
    }

    public function testUpdate()
    {
        $lieu = $this->lieuRepository->find($this->getLastId());
        $lieu->setNom('TestAfterUpdate');
        $this->entityManager->persist($lieu);
        $this->entityManager->flush();
        $lastLieuAfterUpdate = $this->lieuRepository->find($this->getLastId());
        $this->assertEquals($lieu->getNom(), $lastLieuAfterUpdate->getNom());
    }

    public function testDelete()
    {
        $lieu = $this->lieuRepository->findAll();
        $sizeBeforeDelete = count($lieu);
        $lieuToRemove = $this->lieuRepository->find($this->getLastId());
        $this->entityManager->remove($lieuToRemove);
        $this->entityManager->flush();
        $lieuAfterDelete = $this->lieuRepository->findAll();
        $sizeAfterDelete = count($lieuAfterDelete);
        $this->assertEquals($sizeBeforeDelete, $sizeAfterDelete + 1);
    }

    private function getLastId()
    {
        $lastLieu = $this->lieuRepository->findBy([], ['id' => 'DESC'], 1, 0);
        return $lastLieu[0]->getId();
    }

    private function lieuBuilder($data, $em)
    {
        $lieu = new Lieu();

        $ville = new Ville();
        $ville->setNom('BREST');
        $ville->setCodepostal('29000');
        $em->persist($ville);
        $em->flush();

        $lieu->setNom($data['nom'] ?? 'nouveau lieu');
        $lieu->setRue($data['rue'] ?? 'nouvelle rue');
        $lieu->setLatitude($data['lat'] ?? '59,22256');
        $lieu->setLongitude($data['long'] ?? '-5845172');
        $lieu->setVille($ville);

        return $lieu;

    }
}
