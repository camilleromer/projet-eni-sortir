<?php


namespace App\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class VilleControllerFunctionalTest extends WebTestCase
{
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testDeleteVille()
    {

    $this->client = static::createClient();
    $this->client->request(
        'POST',
        '/ville/manage/delete/0',
        array(),
        array(),
        array('CONTENT_TYPE' => 'application/json'),
        '[{"villeId":"1"}]'
    );
    $this->assertJsonResponse($this->client->getResponse(), 201);

    }
    protected function assertJsonResponse($response, $statusCode = 200)
    {
        $this->assertEquals(
            $statusCode, $response->getStatusCode(),
            $response->getContent()
        );
        $this->assertTrue(
            $response->headers->contains('Content-Type', 'application/json'),
            $response->headers
        );
    }
}