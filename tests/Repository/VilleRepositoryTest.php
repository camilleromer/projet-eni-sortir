<?php


namespace App\Tests\Repository;
use App\Entity\Ville;
use App\Tests\FixtureAwareTestCase;
USE  Doctrine\Persistence\ManagerRegistry;

class VilleRepositoryTest extends  FixtureAwareTestCase
{
    /**
     * @var ManagerRegistry
     */
    private $entityManager;
    private $villeRepository;

    protected function setUp()
    {
        parent::setUp();
        $kernel = static::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->villeRepository = $this->entityManager->getRepository(Ville::class);
    }

    public function testFind(): void
    {
        $ville = $this->villeRepository->find(1);
        $this->assertEquals('Rennes', $ville->getNom());
    }

    public function testFindVillesByName(): void
    {
        $villeExpected = new Ville();
        $villeExpected->setNom('Rennes');
        $villeExpected->setCodepostal('35000');
        $actualVille = $this->villeRepository->findVillesByName('Rennes');

        $this->assertEquals($villeExpected->getNom(),$actualVille[0]->getNom());
    }

    public function testFindVilleWithSameProperty(): void
    {
        $actualVille = $this->villeRepository->findVilleWithSameProperty('codepostal', '44000',2);
        $this->assertSame(0,count($actualVille));
    }
}