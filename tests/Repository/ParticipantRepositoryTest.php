<?php

namespace App\Tests\Repository;

use App\Entity\Campus;
use App\Entity\Participant;
use App\Repository\ParticipantRepository;
use App\Tests\FixtureAwareTestCase;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class ParticipantRepositoryTest extends FixtureAwareTestCase
{
    /**
     * @var ManagerRegistry
     */
    private $entityManager;
    private $participantRepository;

    protected function setUp()
    {
        parent::setUp();
        $kernel = static::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->participantRepository = $this->entityManager->getRepository(Participant::class);
    }

    public function testFindUsersWithRelatedEntities(): void
    {
        $user = $this->participantRepository->find(1);
        $this->assertEquals('admin', $user->getPseudo());
        $this->assertEquals('RENNES', $user->getCampus()->getNom());

    }

    public function testGetUsersExceptHimself(){
        $user = $this->participantRepository->find(1);
        $usersToExpect = [];
        $user2 = $this->participantRepository->find(2);
        $user3 = $this->participantRepository->find(3);
        $user4 = $this->participantRepository->find(4);
        $user5 = $this->participantRepository->find(5);
        $usersToExpect[] = $user2;
        $usersToExpect[] = $user3;
        $usersToExpect[] = $user4;
        $usersToExpect[] = $user5;

        $this->assertEquals($usersToExpect,$this->participantRepository->getUsersExceptHimself($user));

    }

    public function userBuilder($data)
    {
        $encoder = $this->createMock(UserPasswordEncoderInterface::class);
        $password = $encoder->encodePassword(new Participant(), 'root');
        $campus = new Campus();
        $campus->setNom('RENNES');
        $user = new Participant();
        $user->setPseudo('pseudo');
        $user->setNom('nom');
        $user->setPrenom('prenom');
        $user->setTelephone('tel');
        $user->setEmail('camille.romer@gmail.com');
        $user->setCampus($campus);
        $user->setActif(true);
        $user->setPassword($password);
        $user->setRoles(array('ROLE_USER'));
        return $user;
    }
}
