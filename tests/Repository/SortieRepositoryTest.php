<?php

namespace App\Tests\Repository;

use App\Entity\Sortie;
use App\Repository\SortieRepository;
use App\Tests\FixtureAwareTestCase;

class SortieRepositoryTest extends FixtureAwareTestCase
{
    protected function setUp()
    {
        parent::setUp();
        $kernel = static::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->sortieRepository = $this->entityManager->getRepository(Sortie::class);
    }

    public function testFindUsersWithRelatedEntities(): void
    {
        $sortie = $this->sortieRepository->find(1);
        $this->assertEquals('Philo', $sortie->getNom());
        $this->assertEquals('admin', $sortie->getOrganisateur()->getPseudo());
        $this->assertEquals('Passée', $sortie->getEtat()->getLibelle());
        $this->assertEquals('Annexe université', $sortie->getLieu()->getNom());
        $this->assertEquals('RENNES', $sortie->getCampus()->getNom());
    }

    public function testFindSortiesWithFilters(){
        $sortie = $this->sortieRepository->find(1);
        $user = $sortie->getOrganisateur();
        $filters = [
            'campus' => $user->getCampus(),
            'nom' => '',
            'date_debut' => '',
            'date_fin' => '',
            'isOrganizer' => true,
            'isRegistered' => true,
            'isNotRegistered' => true,
            'passedOutings' => false
        ];
        $sortiesExpected = [];
        $sortie1 = $this->sortieRepository->find(12);
        $sortie2 = $this->sortieRepository->find(11);
        $sortie3 = $this->sortieRepository->find(2);
        $sortiesExpected[] = $sortie1;
        $sortiesExpected[] = $sortie2;
        $sortiesExpected[] = $sortie3;
        $this->assertEquals($sortiesExpected,$this->sortieRepository->findSortiesWithFilters($filters,$user));
    }

}
