<?php


namespace App\Tests\Repository;
use App\Entity\Campus;
use App\Entity\Ville;
use App\Tests\FixtureAwareTestCase;
USE  Doctrine\Persistence\ManagerRegistry;

class CampusRepositoryTest extends  FixtureAwareTestCase
{
    /**
     * @var ManagerRegistry
     */
    private $entityManager;
    private $campusRepository;

    protected function setUp()
    {
        parent::setUp();
        $kernel = static::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->campusRepository = $this->entityManager->getRepository(Campus::class);
    }

    public function testFind(): void
    {
        $ville = $this->campusRepository->find(1);
        $this->assertEquals('RENNES', $ville->getNom());
    }

    public function testFindCampusByName(): void
    {
        $campusExpected = new Campus();
        $campusExpected->setNom('RENNES');
        $actualCampus = $this->campusRepository->findCampusByName('RENNES');

        $this->assertEquals($campusExpected->getNom(),$actualCampus[0]->getNom());
    }

    public function testFindCampusWithSameProperty(): void
    {
        $actualCampus = $this->campusRepository->findCampusWithSameProperty('nom', 'RENNES', 1);
        $this->assertSame(0,count($actualCampus));
    }
}