var App = (

    function () {

        var App = {};

        /**
         * Ajoute une notification 'PNotify'
         */
        App.addNotification = function (title, text, nType) {
            new PNotify({
                title: title,
                text: text,
                type: nType,
                delay: 1600,
                //hide:false,
                styling: 'bootstrap3'
            });
        };

        /**
         * Actions liés à une sortie (inscription/désinscription/publication)
         */
        App.actionSortie = function (url) {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'json',
            })
                .done(function (data) {
                    //On notifie l'utilisateur de la réussite
                    App.addNotification(data.notif.title, data.notif.content, 'success');

                    // On rafraichit la table
                    $('#tableBlock').empty();
                    $('#tableBlock').html(data.table);
                    adaptIHM();
                })
                .fail(function (xhr) {
                        //Cas où l'appel a pu se faire mais dans le retour API on renvoie une erreur
                        if (typeof xhr.responseJSON !== "undefined") {
                            var notif = xhr.responseJSON.message;
                            if (notif !== '') {
                                App.addNotification(notif.title, notif.content, 'error');
                                //Si pas de message spécifique de détecté on envoie un message d'erreur global
                            } else {
                                App.addNotification("Erreur", "Une erreur est survenue", "error");
                            }
                            //Cas où l'appel n'a même pas pu se faire (exemple si erreur 404, si csrf failed check...)
                        } else {
                            App.addNotification("Erreur", "Erreur " + xhr.status + " : " + xhr.statusText, "error");
                        }
                    }
                );
        }


        /**
         * Actions relatives aux campus (ajout/modif/suppression)
         */
        App.actionCampus = function (action, url, campusId=null) {

            var data = ''
            if(action === 'delete'){
                $('#campusId').val(campusId);
                data = $('#deleteFormCampus').serialize();

            }else if(action === 'update'){
                data = $('#formModifCampus').serialize();

            }else if(action === 'add'){
                data = $('#formAddCampus').serialize()
            }
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data:data
            })
                .done(function (data) {
                    if(typeof data.notif.type !== 'undefined'){
                        App.addNotification(data.notif.title, data.notif.content,  "error");
                    }else{
                        App.addNotification(data.notif.title, data.notif.content, 'success');
                    }
                    // On rafraichit la table
                    $('#tableCampus').empty();
                    $('#tableCampus').html(data.table);
                })
                .fail(function (xhr) {
                        //Cas où l'appel a pu se faire mais dans le retour API on renvoie une erreur
                        if (typeof xhr.responseJSON !== "undefined") {
                            if (typeof xhr.responseJSON.message !== "undefined") {
                                var notif = xhr.responseJSON.message;
                                if (notif !== '') {
                                    App.addNotification(notif.title, notif.content, 'error');
                                    //Si pas de message spécifique de détecté on envoie un message d'erreur global
                                } else {
                                    App.addNotification("Erreur", "Une erreur est survenue", "error");
                                }
                            } else {
                                App.addNotification("Erreur", "Erreur " + xhr.status + " : " + xhr.statusText, "error");
                            }
                            //Cas où l'appel n'a même pas pu se faire (exemple si erreur 404, si csrf failed check...)
                        } else {
                            App.addNotification("Erreur", "Erreur " + xhr.status + " : " + xhr.statusText, "error");
                        }
                    }
                );
        }

        /**
         * Actions relatives aux villes (ajout/modif/suppression)
         */
        App.actionVilles = function (action, url, villeId=null) {

            var data = ''
            if(action === 'delete'){
                $('#villeId').val(villeId);

                data = $('#deleteFormVille').serialize();

            }else if(action === 'update'){
                data =  {"nomVille" : $('#villeName').val(), "cpVille": $('#villeCp').val()}
            }else if(action === 'add'){
                data =  {"nomVille" : $('#villeName').val(), "cpVille": $('#cpName').val()}

            }
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data:data
            })
                .done(function (data) {
                    if(typeof data.notif.type !== 'undefined'){
                        App.addNotification(data.notif.title, data.notif.content,  "error");
                    }else{
                        App.addNotification(data.notif.title, data.notif.content, 'success');
                    }
                    // On rafraichit la table
                    $('#tableVilles').empty();
                    $('#tableVilles').html(data.table);
                })
                .fail(function (xhr) {
                        //Cas où l'appel a pu se faire mais dans le retour API on renvoie une erreur
                        if (typeof xhr.responseJSON !== "undefined") {
                            if (typeof xhr.responseJSON.message !== "undefined") {
                                var notif = xhr.responseJSON.message;
                                if (notif !== '') {
                                    App.addNotification(notif.title, notif.content, 'error');
                                    //Si pas de message spécifique de détecté on envoie un message d'erreur global
                                } else {
                                    App.addNotification("Erreur", "Une erreur est survenue", "error");
                                }
                            } else {
                                App.addNotification("Erreur", "Erreur " + xhr.status + " : " + xhr.statusText, "error");
                            }
                            //Cas où l'appel n'a même pas pu se faire (exemple si erreur 404, si csrf failed check...)
                        } else {
                            App.addNotification("Erreur", "Erreur " + xhr.status + " : " + xhr.statusText, "error");
                        }
                    }
                );
        }

        /**
         * Affiche les informations sur le lieu
         */
        App.displayLieuInfos = function (url) {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'json',
            })
                .done(function (data) {
                    var rue = (data.lieu.rue != null) ? data.lieu.rue : 'Non renseigné';
                    var latitude = (data.lieu.latitude != null) ? data.lieu.latitude : 'Non renseigné';
                    var longitude = (data.lieu.longitude != null) ? data.lieu.longitude : 'Non renseigné';

                    $('#infosLieu').empty();
                    $('#infosLieu').append(
                        '<div class="form-group"><label>Rue : </label><label>' + rue + '</label></div>' +
                        '<div class="form-group"><label>Code postal :</label><label>' + data.lieu.ville + '</label></div>' +
                        '<div class="form-group"><label>Latitude :</label><label>' + latitude + '</label></div>' +
                        '<div class="form-group"><label>Longitude :</label><label>' + longitude + '</label></div>'
                    )
                })
                .fail(function (xhr) {
                    if (xhr.status !== 0) {
                        App.addNotification("Erreur", "Erreur " + xhr.status + " : " + xhr.statusText, "error");
                    }
                });

        }
        /**
         * Permet de remplir le select des lieux dynamiquement en fonction de la ville choisie
         * @param url
         */
        App.hydrateSelectPlaces = function (url) {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'json',
            })
                .done(function (data) {
                    $('#sortie_lieu').empty();
                    if (data.lieux.length === 0) {
                        $('#sortie_lieu').append(
                            '<option value="">Veuillez ajouter un lieu</option>'
                        )
                    }
                    $.each(data.lieux, function (key, value) {
                        $('#sortie_lieu').append(
                            '<option value="' + value.id + '">' + value.nom + '</option>'
                        )
                    })
                })
                .fail(function (xhr) {
                    if (xhr.status !== 0) {
                        App.addNotification("Erreur", "Erreur " + xhr.status + " : " + xhr.statusText, "error");
                    }

                });
        }
        /**
         * Suppression d'un participant
         */
        App.deleteParticipant = function (url, userId) {
            $('#userId').val(userId);
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: $('#deleteForm').serialize()
            })
                .done(function (data) {
                    //On notifie l'utilisateur de la réussite
                    App.addNotification(data.notif.title, data.notif.content, 'success');
                    // On rafraichit la table
                    $('#usersTable').empty();
                    $('#usersTable').html(data.table);
                })
                .fail(function (xhr) {
                        //Cas où l'appel a pu se faire mais dans le retour API on renvoie une erreur
                        if (typeof xhr.responseJSON !== "undefined") {
                            if (typeof xhr.responseJSON.message !== "undefined") {
                                var notif = xhr.responseJSON.message;
                                if (notif !== '') {
                                    App.addNotification(notif.title, notif.content, 'error');
                                    //Si pas de message spécifique de détecté on envoie un message d'erreur global
                                } else {
                                    App.addNotification("Erreur", "Une erreur est survenue", "error");
                                }
                            } else {
                                App.addNotification("Erreur", "Erreur " + xhr.status + " : " + xhr.statusText, "error");

                            }
                            //Cas où l'appel n'a même pas pu se faire (exemple si erreur 404, si csrf failed check...)
                        } else {
                            App.addNotification("Erreur", "Erreur " + xhr.status + " : " + xhr.statusText, "error");
                        }
                    }
                );
        }
        /**
         * Activation/désactivation d'un participant par l'admin
         */
        App.disableOrEnable = function (url) {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'json',
            })
                .done(function (data) {
                    //On notifie l'utilisateur de la réussite
                    App.addNotification(data.notif.title, data.notif.content, 'success');
                    // On rafraichit la table
                    $('#tableParticipant').empty();
                    $('#tableParticipant').html(data.table);
                })
                .fail(function (xhr) {
                        //Cas où l'appel a pu se faire mais dans le retour API on renvoie une erreur
                        if (typeof xhr.responseJSON !== "undefined") {
                            if (typeof xhr.responseJSON.message !== "undefined") {
                                var notif = xhr.responseJSON.message;
                                if (notif !== '') {
                                    App.addNotification(notif.title, notif.content, 'error');
                                    //Si pas de message spécifique de détecté on envoie un message d'erreur global
                                } else {
                                    App.addNotification("Erreur", "Une erreur est survenue", "error");
                                }
                            } else {
                                App.addNotification("Erreur", "Erreur " + xhr.status + " : " + xhr.statusText, "error");

                            }
                            //Cas où l'appel n'a même pas pu se faire (exemple si erreur 404, si csrf failed check...)
                        } else {
                            App.addNotification("Erreur", "Erreur " + xhr.status + " : " + xhr.statusText, "error");
                        }
                    }
                );
        }


        return App;
    })();
